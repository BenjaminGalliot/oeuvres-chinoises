# Résumé

Projet personnel pour récupérer et structurer des œuvres chinoises, notamment le 聊齋志異, à l'aide d'une petite base de données, notamment afin de pouvoir facilement faire des requêtes diverses sur les contes, d’en tirer des version bilingues (deux langues en regard), voire d'éditer des livres numériques à usage personnel avec une structure interne propre, et donc facilement modifiable selon les goûts de chacun.

# Liste des étapes fonctionnelles

* Extraction des textes bruts chinois (avec leur structure) depuis le site *ctext* par l’interface en JSON, recréation d’un texte enrichi en XML…
* Extraction des textes bruts français depuis le fichier epub (non libre de droits), nettoyage complexe en supprimant les espaces superflus et les éléments HTML inutiles, en déplaçant les espaces entre éléments frères, etc., restructuration en texte enrichi en XML après analyse récursive des éléments HTML, orthotypographie…
* Enrichissement d’une base de données SQLite en y intégrant tous les contes, chapitres et annexes diverses, puis en faisant les correspondances entre les textes originaux et leur traduction à partir des références extraites de ces dernières.
* Récupération de ces textes de la base de données pour en faire des fichiers XML complets (original et traduction), puis, par transformation XSL, création d’un fichier HTML stylisé de bon niveau (textes original et traduit en regard, français soigné avec ligatures esthétiques, chinois vertical disposé en colonnes paginées, etc.), incorporant, sur la gauche, un menu permettant de changer à la volée le style et, sur la droite, un menu permettant de choisir un conte précis parmi ceux disponibles (ces derniers variant selon les paramètres choisis pour l’édition du fichiers personnalisé).

# Note sur les droits d’auteur

Les textes chinois sont libres de droit du fait de leur ancienneté (quelques siècles), ce qui n’est pas le cas des traductions françaises, âgées de quelques décennies pour les plus anciennes. Par conséquent, aucune œuvre n’est disponible sur ce dépôt et seul un conte du Liaozhai (qui en compte environ 500) est disponible à des fins d’illustration technique (voir les images [1](exemples/exemple.png), [2](exemples/exemple suite.png) et [3](exemples/exemple suite et fin.png), le [XML](résultats/publications/Grâce.xml) et le [HTML](résultats/publications/Grâce.html)). Ce dernier a été traduit par feu André Lévy pour les éditions Picquier, qui a réédité l’intégrale en fin d’année 2020 (la première édition sortie en 2005 est très difficile d’accès depuis de nombreuses années).

# Travail en cours et pistes d’amélioration

* Gérer le texte enrichi plus simplement maintenant que les fichiers Epub Picquier sont plus propres.
* Extraire les images Epub pour les associer dans les XML et HTML.
* Permettre une adelphisation (lien entre œuvres associées uniquement par une différence de langue, donc traduction et original) même en cas de différence de profondeur par récursion (typiquement, le 拍案驚奇 qui pourrait regrouper les 2 recueils de *ctext*).
* Mettre la main sur d’autres livres électroniques comportant de nombreux contes, visiblement inexistants :
  * 子不語 (Ce dont le Maître ne parlait pas)
  * 灤陽消夏錄 (Passe-temps d’un été à Luanyang)
* Préremplir manuellement les titres des contes des livres possédés qui n’existent pas en version électronique…
* Attendre les améliorations du W3C pour la gestion du flux orthogonal (chinois vertical paginé)…

# Installation

Outre les modules Julia déjà gérés en interne et donc automatiquement installés, les transformations XSLT 2 passent actuellement par *saxonb-xslt*.

# Utilisation

## Création des fichiers sources (yml)

Le gabarit des fichiers sources est le suivant :

```yml
contenu:
- titre: ergonyme (transcription)
  langue: français|chinois|etc.
  type: métarecueil|recueil|chapitre|conte|etc.
  auteur: anthroponyme avec espaces entre nom et prénom (transcription)
  traducteur: anthroponyme avec espaces entre nom et prénom (transcription)
  référence:
    titre: ergonyme de l’adelphe (transcription)
    œuvre: ergonyme de l’œuvre englobante (transcription)
    position: 1 – 2
    langue: français|chinois|etc.
  contenu: (récursion sur ce qui vient d’être décrit)
```

## Logique

En considérant qu’une œuvre originale est unique, il n’en est pas de même pour les œuvres dérivées telles que les traductions. De plus, et c’est ce qui rend la tâche vite complexe dans notre cas, un ouvrage de traduction d’une de ces œuvres peut être partiel, ou même être un amalgame de différentes œuvres originales. Il est ainsi plus sage de considérer différemment les œuvres originales des traductions, plutôt qu’essayer d’élaborer une symétrie.

Par exemple, le 聊齋誌異 est traduit intégralement (aux contes apocryphes près) par les *Chroniques de l’Étrange* (Picquier), ainsi nous avons une symétrie pertinente, mais si nous prenons le 子不語, il n’en existe pas de traduction intégrale en français, mais *Ce dont le Maître ne parlait pas* (Gallimard) s’est concentré sur les contes liés aux rêves, et il existe quelques autres traductions de contes, notamment publiés dans *Ce dont le Maître ne parle pas* (Visage vert), qui comprend aussi une traduction d’un conte du 續子不語, un autre recueil lié au précédent. Notons aussi que le 太平廣記 n’a eu que des traductions très partielles, que *L’Amour de la renarde* traduit lui aussi quelques contes issus du 初刻拍案驚奇 et du 二刻拍案驚奇…

Les relations entre œuvres originales et traduites n’étant pas bijectives, j’ai fait le choix de permettre à chaque traduction et à ses sous-parties de pouvoir se référer à n’importe quel niveau de profondeur de l’œuvre originale. Ainsi, les mélanges et extraits partiels devraient être possibles, puisqu’il est illusoire d’espérer avoir des traductions intégrales, étant donné la tailles des œuvres originales…
