module Générateur

using ..Général
using ..Textes
using ..BaseDeDonnées
using DataFrames
using DocStringExtensions
using Downloads
using FilePaths
using FilePathsBase: /
using LightXML
using Pipe
using ZipFile

export exporter, générer_analecta

"""
$(TYPEDSIGNATURES)
Génère paramétriquement un analecta en créant des documents *XML* et *HTML*.
"""
function générer_analecta(tableau::DataFrame, titre::AbstractString="analecta"; style_incorporé::Bool=true)::Union{SystemPath, Nothing}
    chemin = titre |> Path
    @info "Avec ces critères, cette œuvre nommée « $titre » sera composée d’un nombre de contes égal à $(size(tableau, 1))."
    document_xml = générer_œuvre_xml(tableau, titre)
    document_html = générer_document_html(titre; style_incorporé)
    return document_html
end

"""
$(TYPEDSIGNATURES)
Génère le fichier *XML* d’un analecta.
"""
function générer_œuvre_xml(tableau::DataFrame, titre::AbstractString, chemin_cible::SystemPath=Général.configuration["génération"]["chemins"]["cible"])::XMLDocument
    document = XMLDocument()
    racine = create_root(document, "document")
    nœud_informations_générales = new_child(racine, "informations")
    nœud_analecta = new_child(nœud_informations_générales, "titre")
    add_text(nœud_analecta, titre)
    nœud_dyades = new_child(racine, "dyades")
    for (position, dyade) ∈ eachrow(tableau) |> enumerate
        nœud_dyade = new_child(nœud_dyades, "dyade")
        set_attributes(nœud_dyade, id=position)
        for (adelphe, suffixe) ∈ zip(["traduction", "original"], ["_œuvre_1", "_œuvre_2"])
            données_œuvre = dyade[suffixe |> Regex] |> DataFrame
            rename!(données_œuvre, @pipe données_œuvre |> names .|> replace(_, suffixe => "") .|> Symbol)
            données_œuvre = données_œuvre |> only
            nœud_œuvre = new_child(nœud_dyade, "œuvre")
            set_attributes(nœud_œuvre, type=adelphe, langue=données_œuvre.code_xml)
            nœud_titre = new_child(nœud_œuvre, "titre")
            set_attributes(nœud_titre, langue=données_œuvre.code_xml)
            add_text(nœud_titre, données_œuvre.titre)
            if !ismissing(données_œuvre.nom)
                nœud_auteur = new_child(nœud_œuvre, "auteur")
                set_attributes(nœud_auteur, langue=données_œuvre.code_xml)
                nœud_prénom = new_child(nœud_auteur, "prénom")
                set_attributes(nœud_prénom, langue=données_œuvre.code_xml)
                add_text(nœud_prénom, données_œuvre.prénom)
                nœud_nom = new_child(nœud_auteur, "nom")
                set_attributes(nœud_nom, langue=données_œuvre.code_xml)
                add_text(nœud_nom, données_œuvre.nom)
            end
            if !ismissing(données_œuvre.texte)
                nœud_corps = new_child(nœud_œuvre, "corps")
                for paragraphe ∈ données_œuvre.texte
                    nœud_paragraphe = new_child(nœud_corps, "paragraphe")
                    set_attributes(nœud_paragraphe, langue=données_œuvre.code_xml)
                    add_text(nœud_paragraphe, paragraphe)
                end
            end
            if !ismissing(données_œuvre.notes)
                nœud_notes = new_child(nœud_œuvre, "notes")
                for note ∈ données_œuvre.notes
                    nœud_note = new_child(nœud_notes, "note")
                    set_attributes(nœud_note, langue=données_œuvre.code_xml)
                    add_text(nœud_note, note)
                end
            end
            if !ismissing(données_œuvre.glossaire)
                nœud_glossaire = new_child(nœud_œuvre, "glossaire")
                for note ∈ données_œuvre.glossaire
                    nœud_note = new_child(nœud_glossaire, "note")
                    set_attributes(nœud_note, langue=données_œuvre.code_xml)
                    nœud_vedette = new_child(nœud_note, "vedette")
                    add_text(nœud_vedette, note["vedette"])
                    nœud_définition = new_child(nœud_note, "définition")
                    add_text(nœud_définition, note["définition"])
                end
            end
            if !ismissing(données_œuvre.parents)
                nœud_bibliographie = new_child(nœud_œuvre, "bibliographie")
                for parent ∈ données_œuvre.parents
                    nœud_parent = new_child(nœud_bibliographie, "parent")
                    set_attributes(nœud_parent, type=parent.type)
                    add_text(nœud_parent, parent.titre)
                end
            end
        end
    end
    écrire(document, chemin_cible / "$titre.xml" |> Path)
    return document
end

"""
$(TYPEDSIGNATURES)
Génère le fichier *HTML* d’une œuvre.
"""
function générer_document_html(titre::AbstractString, chemin_style::SystemPath=Général.configuration["génération"]["chemins"]["style"], chemin_source::SystemPath=Général.configuration["génération"]["chemins"]["cible"], chemin_cible::SystemPath=Général.configuration["génération"]["chemins"]["cible"]; style_incorporé)::Union{SystemPath, Nothing}
    document = transformer_par_xsl(abspath(chemin_source / titre * ".xml"), abspath(chemin_style), abspath(chemin_cible / titre * ".html"); style_incorporé)
    return document
end

"""
$(TYPEDSIGNATURES)
Réalise une transformation *XSL* pour une feuille de style donnée.
"""
function transformer_par_xsl(chemin_source::SystemPath, chemin_style::SystemPath, chemin_cible::SystemPath; style_incorporé::Bool=false)::Union{SystemPath, Nothing}
    exécutable = détecter_exécutable_xslt()
    tampon_erreur = IOBuffer()
    résultat = @pipe try
        commande = `$exécutable -s:$chemin_source -xsl:$chemin_style style_incorporé=$style_incorporé`
        @info("Commande XSLT : $commande")
        résultat = @pipe commande |> Cmd(_, dir=parent(chemin_cible) |> string) |> pipeline(_; stderr=tampon_erreur) |> read(_, String)
    catch exception
        if exception isa ProcessFailedException
            message = tampon_erreur |> take! |> String
            @error("Erreur de compilation XSL :\n$message")
;        elseif exception isa Base.IOError
            @error("Erreur d’entrée/sortie (code $(exception.code)) :\n$(exception.msg)")
        else
            error(exception)
        end
    end
    if !isnothing(résultat)
        @pipe résultat |> nettoyer_html |> écrire(_, chemin_cible)
        résultat = chemin_cible
    end
    return résultat
end

"""
$(TYPEDSIGNATURES)
Nettoie un texte *HTML* issu d’une transformation *XSL" incluant des fichiers annexes.
"""
function nettoyer_html(document::AbstractString; modèle::Regex=r"^(?<indentation>\s+)<(?<balise>style|script).*?>\n(?<contenu>.+?)<\/(?P=balise)>$"sm, indentation::Int=4)::String
    document = replace(document, "   " => " " ^ indentation)
    bilans = eachmatch(modèle, document)
    for bilan ∈ bilans
        contenu = String[]
        taille_indentation = (bilan["indentation"] |> length) + indentation
        for ligne ∈ @pipe bilan["contenu"] |> split(_, "\n")
            push!(contenu, " " ^ taille_indentation * ligne)
        end
        contenu = join(contenu, "\n")
        document = replace(document, bilan["contenu"] => contenu)
    end
    return document
end

"""
$(TYPEDSIGNATURES)
Détecte quel est l’exécutable disponible pour la transformation *XSL*.
"""
function détecter_exécutable_xslt()::Cmd
    saxon_présent = @pipe `whereis saxonb-xslt` |> read(_, String) |> split |> filter(!endswith(":"), _) |> !isempty
    if saxon_présent
        commande = `saxonb-xslt`
    else
        commande = retry(obtenir_transformateur_xsl, delays=Base.ExponentialBackOff(n=3, first_delay=5, max_delay=1000))()
    end
    return commande
end

"""
$(TYPEDSIGNATURES)
Obtient un transformateur *XSL*, en en installant un si nécessaire (*Saxon-HE* privilégié).
"""
function obtenir_transformateur_xsl(lien::AbstractString="https://sourceforge.net/projects/saxon/files/Saxon-HE/10/Java/SaxonHE10-5J.zip/download", modèle::Regex=r"saxon-he-[\d.]+\.jar", cible::SystemPath=p"exécutables/transformer"; toujours_installer::Bool=false, nombre_essais=5)
    if toujours_installer || !isfile(cible) || filesize(cible) == 0
        cible |> parent |> mkpath
        lecteur = @pipe lien |> Downloads.download |> ZipFile.Reader
        chemin = @pipe lecteur.files |> filter(fichier -> match(modèle, fichier.name) |> !isnothing, _) |> only
        open(cible, "w") do fichier
            @pipe chemin |> read |> write(fichier, _)
        end
        chmod(cible, 0o777)
    end
    cible = cible |> abspath
    commande = `java -jar $cible`
    return commande
end

"""
$(TYPEDSIGNATURES)
Exporte un tableau de résultats au format CSV.
"""
function exporter(tableau::DataFrame, chemin::AbstractString)
    écrire(tableau, chemin |> Path)
end

end
