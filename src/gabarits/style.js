function afficher_volet(identifiant) {
    var éléments_menu = document.querySelectorAll("#" + identifiant + " ul.visible, " + "#" + identifiant + " ul.invisible");
    Array.prototype.forEach.call(éléments_menu, function(élément) {
        élément.classList.toggle("visible");
        élément.classList.toggle("invisible");
    });
    index = document.getElementById("index")
    profondeur = window.getComputedStyle(index).getPropertyValue("z-index");
    délai = profondeur == 3 ? 500 : 0;
    setTimeout(function(){
        index.style.zIndex = profondeur == 3 ? 1 : 3;
    }, délai);
}

function changer_fond() {
    image = window.getComputedStyle(document.body).getPropertyValue("background-image")
    lien = 'url("http://api.thumbr.it/whitenoise-361x370.png?background=fcf1d5&noise=626262&density=15&opacity=15")'
    document.body.style.backgroundImage = image == lien ? "none" : lien
}

function changer_renvois() {
    var renvois = document.querySelectorAll(".renvoi");
    Array.prototype.forEach.call(renvois, function(élément) {
        affichage = window.getComputedStyle(élément).getPropertyValue("display");
        élément.style.display = affichage == "inline" ? "none" : "inline";
    });
}

function changer_disposition() {
    var blocs = document.getElementsByClassName("œuvre");
    Array.prototype.forEach.call(blocs, function(élément) {
        côté = window.getComputedStyle(élément).getPropertyValue("float");
        élément.style.float = côté == "right" ? "left" : "right";
    });
}

function changer_nombre_colonnes(classe) {
    var textes = document.getElementsByClassName(classe);
    Array.prototype.forEach.call(textes, function(élément) {
        nombre_colonnes = window.getComputedStyle(élément).getPropertyValue("column-count");
        élément.style.columnCount = nombre_colonnes == 1 ? 2 : 1;
    });
}

function changer_sens_lecture(classe) {
    var textes = document.getElementsByClassName(classe);
    Array.prototype.forEach.call(textes, function(élément) {
        sens = window.getComputedStyle(élément).getPropertyValue("writing-mode");
        élément.style.writingMode = sens == "vertical-rl" ? "horizontal-tb" : "vertical-rl";
    });
}

function changer_police(classe) {
    var textes = document.getElementsByClassName(classe);
    Array.prototype.forEach.call(textes, function(élément) {
        police = window.getComputedStyle(élément).getPropertyValue("font-family");
        if (classe == "original") {
            élément.style.fontFamily = police.includes("AR PL KaitiM Big5") ? "'AR PL Mingti2L Big5', 'I.MingVarCP', 'STFangsong', 'Noto Sans TC', sans-serif" : "'AR PL KaitiM Big5', 'Kaiti TC', 'Noto Serif TC', serif";
        } else {
            élément.style.fontFamily = police.includes("CormorantUprightRegular") ? "'CormorantGaramondRegular', serif" : "'CormorantUprightRegular', serif";
        }
    });
}

function changer_ligatures(classe) {
    var textes = document.getElementsByClassName(classe);
    Array.prototype.forEach.call(textes, function(élément) {
        ligatures = window.getComputedStyle(élément).getPropertyValue("font-variant-ligatures");
        élément.style.fontVariantLigatures = ligatures == "common-ligatures discretionary-ligatures historical-ligatures contextual" ? "normal" : "common-ligatures discretionary-ligatures historical-ligatures contextual";
    });
}

function choisir_œuvre(identifiant) {
    var œuvres = document.getElementsByClassName("dyade");
    Array.prototype.forEach.call(œuvres, function(élément) {
        élément.style.display = élément.id == identifiant || identifiant == "TOUS" ? "block" : "none";
    });
    if (identifiant == "TOUS") {
        window.scrollTo(0, 0);
    } else {
        élément = document.getElementById(identifiant);
        élément.scrollIntoView({behavior: "smooth"});
    }
}
