<?xml version="1.0" encoding="utf-8" ?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="2.0" xml:lang="fr">
<xsl:output method="html" doctype-public="-//W3C//DTD HTML 5 Transitional//FR" encoding="utf-8" indent="yes"/>
<xsl:param name="style_incorporé"/>

<xsl:template match="document">
    <head>
        <link rel="stylesheet" media="screen" href="https://fontlibrary.org/face/cormorant" type="text/css"/>
        <link href="https://fonts.googleapis.com/css2?family=Noto+Serif+TC:wght@200;300;400;500;600;700;900&amp;display=swap" rel="stylesheet"/>
        <link href="https://fonts.googleapis.com/css2?family=Noto+Sans+TC:wght@100;300;400;500;700;900&amp;display=swap" rel="stylesheet"/>
        <xsl:choose>
            <xsl:when test="$style_incorporé='true'">
                <style type="text/css">
                    <xsl:text>&#xa;</xsl:text>
                    <xsl:copy-of select="unparsed-text('style.css')"/>
                </style>
            </xsl:when>
            <xsl:otherwise>
                <link type="text/css" rel="stylesheet" href="style.css"/>
            </xsl:otherwise>
        </xsl:choose>
        <title>
            <xsl:value-of select="informations/titre"/>
        </title>
    </head>
    <xsl:text>&#xa;</xsl:text>
    <body>
        <xsl:call-template name="menu"/>
        <xsl:call-template name="index"/>
        <xsl:apply-templates select="dyades"/>
        <xsl:choose>
            <xsl:when test="$style_incorporé='true'">
                <xsl:text>&#xa;</xsl:text>
                <script type="text/javascript">
                    <xsl:text>&#xa;</xsl:text>
                    <xsl:copy-of select="unparsed-text('style.js')"/>
                </script>
                <xsl:text>&#xa;</xsl:text>
            </xsl:when>
            <xsl:otherwise>
                <script type="text/javascript" src="style.js"></script>
            </xsl:otherwise>
        </xsl:choose>
    </body>
</xsl:template>

<xsl:template name="menu">
    <div id="menu">
        <p onclick="afficher_volet('menu')">☰</p>
        <ul class="invisible">
            <li><p>général</p>
                <ul>
                    <li onclick="changer_fond()">fond</li>
                    <li onclick="changer_renvois()">renvois</li>
                    <li onclick="changer_disposition()">disposition</li>
                </ul>
            </li>
            <li><p>français</p>
                <ul>
                    <li onclick="changer_nombre_colonnes('traduction')">colonnes</li>
                    <li onclick="changer_police('traduction')">police</li>
                    <li onclick="changer_ligatures('traduction')">ligatures</li>
                </ul>
            </li>
            <li><p>chinois</p>
                <ul>
                    <li onclick="changer_nombre_colonnes('original')">colonnes</li>
                    <li onclick="changer_sens_lecture('original')">sens</li>
                    <li onclick="changer_police('original')">police</li>
                </ul>
            </li>
        </ul>
    </div>
</xsl:template>

<xsl:template name="index">
    <div id="index">
        <p onclick="afficher_volet('index')">☷</p>
        <ul class="invisible">
            <li onclick="choisir_œuvre('TOUS')"><em>Tous les contes</em></li>
            <xsl:for-each select="dyades/dyade">
                <li>
                    <xsl:attribute name="onclick">
                        <xsl:text>choisir_œuvre('</xsl:text>
                        <xsl:value-of select="@id"/>
                        <xsl:text>')</xsl:text>
                    </xsl:attribute>
                    <xsl:value-of select="@id"/>
                    <xsl:text> : </xsl:text>
                    <xsl:value-of select="œuvre[@type='traduction']/titre"/>
                    <xsl:text> – </xsl:text>
                    <xsl:value-of select="œuvre[@type='original']/titre"/>
                </li>
            </xsl:for-each>
        </ul>
    </div>
</xsl:template>

<xsl:template match="dyades">
    <xsl:apply-templates select="dyade"/>
</xsl:template>

<xsl:template match="dyade">
    <xsl:element name="div">
        <xsl:attribute name="id">
            <xsl:value-of select="@id"/>
        </xsl:attribute>
        <xsl:attribute name="class">
            <xsl:text>dyade</xsl:text>
        </xsl:attribute>
        <xsl:element name="a">
            <xsl:attribute name="class">
                <xsl:text>ancre</xsl:text>
            </xsl:attribute>
            <xsl:attribute name="href">
                <xsl:text>#</xsl:text>
                <xsl:value-of select="@id"/>
            </xsl:attribute>
        </xsl:element>
        <xsl:apply-templates select="œuvre"/>
    </xsl:element>
</xsl:template>

<xsl:template match="œuvre">
    <xsl:element name="div">
        <xsl:attribute name="class">
            <xsl:text>œuvre </xsl:text>
            <xsl:value-of select="@type"/>
        </xsl:attribute>
        <xsl:attribute name="lang">
            <xsl:value-of select="@langue"/>
        </xsl:attribute>
        <xsl:apply-templates select="titre"/>
        <xsl:apply-templates select="corps"/>
        <xsl:apply-templates select="auteur"/>
        <xsl:apply-templates select="glossaire"/>
        <xsl:apply-templates select="bibliographie"/>
    </xsl:element>
</xsl:template>

<xsl:template match="titre">
    <xsl:element name="h2">
        <xsl:value-of select="."/>
    </xsl:element>
</xsl:template>

<xsl:template match="corps">
    <xsl:element name="div">
        <xsl:attribute name="class">
            <xsl:text>corps</xsl:text>
        </xsl:attribute>
        <xsl:apply-templates select="paragraphe"/>
    </xsl:element>
</xsl:template>

<xsl:template match="glossaire">
    <xsl:element name="div">
        <xsl:attribute name="class">
            <xsl:text>glossaire</xsl:text>
        </xsl:attribute>
        <xsl:apply-templates select="note"/>
    </xsl:element>
</xsl:template>

<xsl:template match="note">
    <xsl:element name="p">
        <xsl:apply-templates select="vedette"/>
        <xsl:apply-templates select="définition"/>
    </xsl:element>
</xsl:template>

<xsl:template match="vedette">
    <xsl:element name="span">
        <xsl:attribute name="class">
            <xsl:text>vedette</xsl:text>
        </xsl:attribute>
        <xsl:value-of select="."/>
    </xsl:element>
</xsl:template>

<xsl:template match="définition">
    <xsl:element name="span">
        <xsl:attribute name="class">
            <xsl:text>définition</xsl:text>
        </xsl:attribute>
        <xsl:value-of select="."/>
    </xsl:element>
</xsl:template>

<xsl:template match="auteur[@langue='fr']">
    <xsl:element name="div">
        <xsl:attribute name="class">
            <xsl:text>auteur</xsl:text>
        </xsl:attribute>
        <xsl:element name="p">
            <xsl:apply-templates select="prénom"/>
            <xsl:text> </xsl:text>
            <xsl:apply-templates select="nom"/>
        </xsl:element>
    </xsl:element>
</xsl:template>

<xsl:template match="auteur[@langue='zh']">
    <xsl:element name="div">
        <xsl:attribute name="class">
            <xsl:text>auteur</xsl:text>
        </xsl:attribute>
        <xsl:element name="p">
            <xsl:apply-templates select="nom"/>
            <xsl:apply-templates select="prénom"/>
        </xsl:element>
    </xsl:element>
</xsl:template>

<xsl:template match="prénom">
    <xsl:element name="span">
        <xsl:attribute name="class">
            <xsl:text>prénom</xsl:text>
        </xsl:attribute>
        <xsl:value-of select="."/>
    </xsl:element>
</xsl:template>

<xsl:template match="nom">
    <xsl:element name="span">
        <xsl:attribute name="class">
            <xsl:text>nom</xsl:text>
        </xsl:attribute>
        <xsl:value-of select="."/>
    </xsl:element>
</xsl:template>

<xsl:template match="bibliographie">
    <xsl:element name="div">
        <xsl:attribute name="class">
            <xsl:text>bibliographie</xsl:text>
        </xsl:attribute>
        <xsl:value-of select="if (ancestor::œuvre/@langue = 'fr') then 'Origine : ' else '起源：'"/>
        <xsl:for-each select="reverse(parent)">
            <xsl:value-of select="."/>
            <xsl:if test="position() != last()">
                <xsl:value-of select="if (ancestor::œuvre/@langue = 'fr') then ', ' else '，'"/>
            </xsl:if>
        </xsl:for-each>
    </xsl:element>
</xsl:template>

<xsl:template match="paragraphe">
    <xsl:element name="p">
        <xsl:apply-templates/>
    </xsl:element>
</xsl:template>

</xsl:stylesheet>
