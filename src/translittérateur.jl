module Translittérateur

using CSV
using DataFrames
using DocStringExtensions
using FilePaths
using Pipe
using Unicode

export préparer_translittérations, extranslittérer, intranslittérer

"""
$(TYPEDSIGNATURES)
Prépare les translittérations pinyin vers EFEO (École française d’Extrême-Orient) ainsi que l’expression rationnelle associée.
"""
function préparer_translittérations(chemin::SystemPath = p"configuration/translittérations.csv")
    translittérations_pinyin_efeo = @pipe chemin |> CSV.read(_, DataFrame; header=true)
    translittérations_pinyin_efeo.efeo = @pipe translittérations_pinyin_efeo.efeo .|> split(_, ",") .|> Vector{String}
    global translittérations_efeo_pinyin = Dict(
        efeo => pinyin
        for (pinyin, groupe_efeo) ∈ translittérations_pinyin_efeo |> eachrow
        for sous_groupe_efeo ∈ groupe_efeo
        for efeo ∈ nettoyer_translittérations(sous_groupe_efeo))
    global modèle_efeo = @pipe translittérations_efeo_pinyin |> keys |> collect |> sort(_; by=length, rev=true) |> join(_, "|") |> Regex(_, "i")
    global translittérations_pinyin_efeo = Dict(
        pinyin => @pipe groupe_efeo |> first |> nettoyer_translittérations(_; strict=true) |> first
        for (pinyin, groupe_efeo) ∈ translittérations_pinyin_efeo |> eachrow)
    global modèle_pinyin = @pipe translittérations_pinyin_efeo |> keys |> collect |> sort(_; by=length, rev=true) |> join(_, "|") |> Regex(_, "i")
end

"""
$(TYPEDSIGNATURES)
Duplique les translittérations EFEO particulières et non toujours utilisées (notamment celles commençant par *ng*).
"""
function nettoyer_translittérations(expression::AbstractString; strict::Bool=false)::Vector{String}
    if startswith(expression, "ng")
        translittérations = strict ? [expression[3:end]] : [expression,  expression[3:end]]
    else
        translittérations = [expression]
    end
    return translittérations
end

"""
$(TYPEDSIGNATURES)
Translittère une expression avec capitalisation d’origine (et séparateur au choix).
"""
function translittérer(expression::AbstractString, correspondances::Dict, modèle::Regex; séparateur::AbstractString=" ")::String
    segments = @pipe eachmatch(modèle, expression) .|> getfield(_, :match)
    capitalisations = segments .|> first .|> isuppercase
    translittérations = [correspondances[segment |> lowercase] |> (capitalisation ? uppercasefirst : lowercase) for (capitalisation, segment) ∈ zip(capitalisations, segments)]
    if join(segments) ≠ expression
        segments_inconnus = reduce(replace, segments .=> "_" .^ length.(segments); init=expression)
        @warn("Certains segments n’ont pas pu être translittérés : $segments_inconnus")
    end
    if isempty(séparateur) && length(translittérations) ≥ 2
        translittérations[2:end] = translittérations[2:end] .|> lowercase
    end
    résultat = join(translittérations, séparateur)
    return résultat
end

"""
$(TYPEDSIGNATURES)
Intranslittère (du pinyin vers EFEO) une expression (séparateur au choix).
"""
function intranslittérer(expression::AbstractString; séparateur::AbstractString=" ")::String
    expression_nettoyée = @pipe replace(expression, r"[’']" => " ") |> Unicode.normalize(_; stripmark=true, stripignore=true)
    return translittérer(expression_nettoyée, translittérations_pinyin_efeo, modèle_pinyin; séparateur)
end

"""
$(TYPEDSIGNATURES)
Extranslittère (d’EFEO vers pinyin) une expression (séparateur au choix).
"""
function extranslittérer(expression::AbstractString; séparateur::AbstractString=" ")::String
    return translittérer(expression, translittérations_efeo_pinyin, modèle_efeo; séparateur)
end

préparer_translittérations()

end
