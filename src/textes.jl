module Textes

using ..Général
using ..Rapporteur
using ..Structures
using DocStringExtensions
using EzXML
using FilePaths
using FilePathsBase: /
using HTTP
using LightXML
using Pipe
using ZipFile

export analyser_epub, extraire_ctext, analyser_page, nettoyer!

global remplacements_globaux = Dict{Tuple{Union{LangueAbstraite, Nothing}, Union{SourceAbstraite, Nothing}}, Vector{NamedTuple}}()

"""
$(TYPEDSIGNATURES)
Renvoie un dictionnaire contenant les informations du site [ctext](https://ctext.org/) pour une liste d’identifiants donnée.

# Arguments
- `identifiant` : l’identifiant ctext, commençant généralement par *wb* ou *ws* et suivie de chiffres ;
- `clef` : clef personnelle permettant un meilleur accès au site ;
- `économiser` : permet d’éviter de télécharger la ressource si cette dernière (de même nom) se trouve dans le dossier temporaire ;
- `chemin` : emplacement du dossier temporaire où sont stockées les ressources déjà téléchargées.
"""
function extraire_ctext(identifiants::Vector{<:AbstractString}, clef::AbstractString; économiser::Bool=true, chemin::SystemPath=p"temporaire" / "ctext")::Dict
    résultats = extraire_ctext.(identifiants, clef; économiser, chemin)
    résultat = Dict(
        "titre" => (@pipe [get(résultat, "titre", nothing) for résultat ∈ résultats] |> join(_, "  ")),
        "type" => get(résultats[1], "type", nothing),
        "auteur" => [get(résultat, "auteur", nothing) for résultat ∈ résultats] |> skipmissing |> Set |> collect |> only,
        "contenu" => vcat([get(résultat, "contenu", nothing) for résultat ∈ résultats]...))
end

"""
$(TYPEDSIGNATURES)
Renvoie un dictionnaire contenant les informations du site [ctext](https://ctext.org/) pour un identifiant donné.

# Arguments
- `identifiant` : l’identifiant ctext, commençant généralement par *wb* ou *ws* et suivie de chiffres ;
- `clef` : clef personnelle permettant un meilleur accès au site ;
- `économiser` : permet d’éviter de télécharger la ressource si cette dernière (de même nom) se trouve dans le dossier temporaire ;
- `chemin` : emplacement du dossier temporaire où sont stockées les ressources déjà téléchargées.
"""
function extraire_ctext(identifiant::AbstractString, clef::AbstractString; économiser::Bool=true, chemin::SystemPath=p"temporaire" / "ctext")::Dict
    chemin_données = chemin / "données $identifiant.json"
    chemin_métadonnées = chemin / "métadonnées $identifiant.json"
    if économiser && isfile(chemin_données) && isfile(chemin_métadonnées)
        @info "Des archives pour l’identifiant $identifiant sont déjà présentes, attention si vous voulez les données les plus à jour !"
        (données, métadonnées) = lire.([chemin_données, chemin_métadonnées])
    else
        (données, métadonnées) = récupérer_texte_json.([p"api.ctext.org"] ./ ["gettext", "gettextinfo"] .* "?urn=$identifiant&apikey=$clef")
        if !haskey(données, "error")
            écrire.([données, métadonnées], [chemin_données, chemin_métadonnées])
        else
            @error "L’identifiant « $identifiant » a renvoyé une erreur : $(données["error"]["code"])"
            return Dict(
                "type" => "erreur")
        end
    end
    if haskey(données, "subsections")
        return Dict(
            "type" => "racine",
            "titre" => métadonnées["title"],
            "auteur" => get(métadonnées, "author", missing),
            "contenu" => données["subsections"] .|> string)
    elseif haskey(données, "fulltext")
        return Dict(
            "type" => "branche",
            "titre" => métadonnées["title"],
            "contenu" => données["fulltext"] .|> string)
    end
end

"""
$(TYPEDSIGNATURES)
Renvoie un dictionnaire contenant les textes *HTML* et les images d’une liste de fichiers au format *epub*.
"""
function analyser_epub(chemins::Vector{<:SystemPath})::Dict
    résultats = analyser_epub.(chemins)
    résultat = Dict(
        "fichiers" => vcat([résultat["fichiers"] for résultat ∈ résultats]...),
        "images" => vcat([résultat["images"] for résultat ∈ résultats]...))
end

"""
$(TYPEDSIGNATURES)
Renvoie un dictionnaire contenant les textes *HTML* et les images d’un fichier au format *epub*.
"""
function analyser_epub(chemin::SystemPath)::Dict
    résultat = Dict(
        "fichiers" => Dict[],
        "images" => String[])
    if exists(chemin)
        fichier = open(chemin, "r") |> ZipFile.Reader
        for fichier ∈ sort(fichier.files, by=fichier -> trier_naturellement(fichier.name))
            chemin_fichier = fichier.name |> Path
            if parent(chemin_fichier).segments[end] ∈ ["OEBPS", "image"]
                if extension(chemin_fichier) ∈ ["html", "xhtml"]
                    push!(résultat["fichiers"], Dict(
                        "chemin" => chemin_fichier,
                        # Supprimer cette partie pour qu’EzXML fonctionne avec les termes et non uniquement avec les positions…
                        "contenu" => @pipe fichier |> read(_, String) |> replace(_, """xmlns="http://www.w3.org/1999/xhtml" """ => "")))
                elseif extension(chemin_fichier) == "png"
                    push!(résultat["images"], chemin_fichier)
                end
            end
        end
    else
        @error "Le fichier « $chemin » n’existe pas !"
    end
    return résultat
end

"""
$(TYPEDSIGNATURES)
Récupère le contenu d’un texte *JSON* du site [ctext](https://ctext.org/) en extrayant seulement les informations utiles.
"""
function récupérer_texte_json(lien::SystemPath)::Dict
    @pipe (lien |> faire_requête).body |> String |> lire(_, "json")
end

"""
$(TYPEDSIGNATURES)
Fais une requête HTTP en y adjoignant le nom de protocole.
"""
function faire_requête(lien::SystemPath)::HTTP.Response
    HTTP.request("GET", "http://$lien")
end

"""
$(TYPEDSIGNATURES)
Analyse une page extraite d’un fichier *epub*.
"""
function analyser_page(page::AbstractString)::EzXML.Document
    page |> parsexml
end

"""
$(TYPEDSIGNATURES)
Trie naturellement une expression (pour les chiffres incorporés) en renvoyant un vecteur d’indices (à utiliser dans une fonction de tri).
"""
function trier_naturellement(expression::AbstractString)::Vector
    résultat = []
    modèle = r"\d+"
    push!(résultat, split(expression, modèle) |> join)
    append!(résultat, [parse(Int, bilan.match) for bilan ∈ collect(eachmatch(modèle, expression))])
end

"""
$(TYPEDSIGNATURES)
Nettoie un texte (et donc ses différents contenus) en appliquant dynamiquement les règles du fichier de configuration liées aux langues et aux sources.
"""
function nettoyer!(texte::Texte)::Texte
    remplacements = créer_remplacements(texte.langue, texte.source)
    for champ ∈ [:brut, :notes]
        if (paragraphes = getfield(texte, champ)) |> !isnothing
            setfield!(texte, champ, modifier_texte.(paragraphes, [remplacements]))
        end
    end
    return texte
end

"""
$(TYPEDSIGNATURES)
Nettoie un nom (et donc ses différents contenus) en appliquant dynamiquement les règles du fichier de configuration liées aux langues et aux sources.
"""
function nettoyer!(nom::NomAbstrait)::NomAbstrait
    remplacements = créer_remplacements(nom.langue)
    for champ ∈ [:nom]
        if (expression = getfield(nom, champ)) |> !isnothing
            setfield!(nom, champ, modifier_texte(expression, remplacements))
        end
    end
    return nom
end

"""
$(TYPEDSIGNATURES)
Crée les remplacements à réaliser lors du nettoyage de textes de langue et source spécifiques.
"""
function créer_remplacements(langue::Union{LangueAbstraite, Nothing}=nothing, source::Union{SourceAbstraite, Nothing}=nothing)
    if haskey(remplacements_globaux, (langue, source))
        remplacements = remplacements_globaux[langue, source]
    else
        dictionnaire = Dict(
            String(champ) => merge(
                [hasfield(typeof(structure), champ) ? getfield(structure, champ) : Dict() for structure ∈ [langue, source]]...)
            for champ ∈ [:modèles, :substituts, :affichages, :niveaux])
        remplacements = [(
            nom = nom_modèle,
            règle = dictionnaire["modèles"][nom_modèle] => dictionnaire["substituts"][nom_modèle],
            niveau = dictionnaire["niveaux"][nom_modèle],
            afficher = dictionnaire["affichages"][nom_modèle])
            for nom_modèle ∈ keys(dictionnaire["modèles"]) if haskey(dictionnaire["substituts"], nom_modèle)]
        remplacements_globaux[langue, source] = remplacements
    end
    return remplacements
end

"""
$(TYPEDSIGNATURES)
Modifie une expression selon un ensemble de règles.
# Arguments
- `expression` : l’expression sur laquelle porteront les modifications ;
- `modifications` : modifications avec leurs paramètres (voir [`afficher_correction`](@ref) pour les paramètres) ;
- `afficher` : Affiche (ou non) l’avant-après de l’expression.
"""
function modifier_texte(expression::AbstractString, modifications::AbstractVector{<:NamedTuple}; afficher::Bool=false)::String
    expression_initiale = expression
    for modification ∈ modifications
        expression_corrigée = replace(expression, modification.règle)
        if modification.afficher > 0 && expression ≠ expression_corrigée
            afficher_correction(expression, modification...)
        end
        expression = expression_corrigée
    end
    if afficher
        @info "Avant : « $expression_initiale »\nAprès : « $expression »"
    end
    return expression
end

"""
$(TYPEDSIGNATURES)
Affiche une correction qui a été faite sur une expression.
# Arguments
- `expression` : l’expression originale ;
- `nom` : nom de la correction ;
- `règle` : règle de la correction (source et cible de la correction) ;
- `niveau` : niveau (priorité) de la correction ;
- `afficher` : nombre de caractères à afficher de chaque côté de la source de la correction (si 0, aucun affichage).
"""
function afficher_correction(expression::AbstractString, nom::AbstractString, règle::Pair{<:Union{Regex, AbstractString}, <:AbstractString}, niveau::Int, afficher::Int)::Nothing
    modèle = règle[1] isa Regex ? règle[1] : règle[1] |> Regex
    corrections = eachmatch(modèle, expression) |> collect
    for correction ∈ corrections
        voisinage = récupérer_voisinage(expression, correction.offset, afficher)
        @warn "Correction effectuée sur « $voisinage » ($nom)."
        rapporter(voisinage, nom, niveau)
    end
end

"""
$(TYPEDSIGNATURES)
Calcule et récupère le voisinage d’un caractère donné à une certaine position dans une expression.
# Arguments
- `expression` : l’expression complète, potentiellement très longue ;
- `position` : la position (en unités de code, confer cette [page](https://docs.julialang.org/en/v1/manual/strings/#Unicode-and-UTF-8-1)) du caractère d’intérêt dans l’expression, généralement obtenue par une fonction de recherche (expressions rationnelles, etc.) ;
- `portée` : l’écart autour de ce caractère (en nombre de caractères réels) à considérer comme voisinage, limité aux bords de l’expression ;
- `affiner` : la recherche de voisinage tentera (ou non) de s’arrêter proprement (aux espaces et ponctuations), récupérant ainsi ceux qui seraient partiellement dans le voisinage brut.
# Exemples
```jldoctest; setup = :(using ŒuvresChinoises.Textes: récupérer_voisinage)
julia> position = findfirst(isequal('裔'), "孔生雪笠，聖裔也。為人蘊藉，工詩。")
19
julia> récupérer_voisinage("孔生雪笠，聖裔也。為人蘊藉，工詩。", position, 3, affiner=false)
"笠，聖裔也。為"
julia> récupérer_voisinage("孔生雪笠，聖裔也。為人蘊藉，工詩。", position, 3, affiner=true)
"孔生雪笠，聖裔也。為人蘊藉，"
julia> récupérer_voisinage("Le bachelier Kong Xueli, de la sainte descendance de Confucius…", 21, 4, affiner=false)
"g Xueli, "
julia> récupérer_voisinage("Le bachelier Kong Xueli, de la sainte descendance de Confucius…", 21, 4, affiner=true)
" Kong Xueli, "
```
"""
function récupérer_voisinage(expression::AbstractString, position::Int, portée::Int; affiner::Bool=true)::String
    position_valide = thisind(expression, position)
    bornes_brutes = prevind(expression, position_valide, portée), nextind(expression, position_valide, portée)
    if affiner
        indices_début = @pipe eachmatch(r"[\p{Z}\p{P}]|^", expression) |> collect |> getproperty.(_, :offset)
        indices_fin = @pipe eachmatch(r"[\p{Z}\p{P}]|$", expression) |> collect |> getproperty.(_, :offset)
        indices_bornes = findlast(indice -> indice ≤ bornes_brutes[1], indices_début), findfirst(indice -> indice ≥ bornes_brutes[2], indices_fin)
        bornes_brutes = !isnothing(indices_bornes[1]) ? indices_début[indices_bornes[1]] : bornes_brutes[1], !isnothing(indices_bornes[2]) ? indices_fin[indices_bornes[2]] : bornes_brutes[2]
    end
    bornes = max(firstindex(expression), bornes_brutes[1]), min(lastindex(expression), bornes_brutes[2])
    expression[UnitRange(bornes...)]
end

end
