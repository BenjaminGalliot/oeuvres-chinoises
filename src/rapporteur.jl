module Rapporteur

using ..Général
using DataFrames
using DocStringExtensions
using FilePaths


export créer_rapporteur, ouvrir_rapport, fermer_rapport, rapporter, générer_rapport

struct RapporteurGlobal
    historique::DataFrame
    contexte_actuel::Vector{<:AbstractString}

    """
    $(TYPEDSIGNATURES)
    Structure récupérant tous les avertissements utiles afin de générer un rapport lisible sous forme de tableau.
    """
    function RapporteurGlobal()
        historique = DataFrame(
            chronologie=Int[],
            message=String[],
            type=String[],
            contexte=String[],
            niveau=Int[])
        contexte_actuel = [""]
        new(historique, contexte_actuel)
    end
end

"""
$(TYPEDSIGNATURES)
Crée le rapporteur global.
"""
function créer_rapporteur()
    global rapporteur_global = RapporteurGlobal()
end

"""
$(TYPEDSIGNATURES)
Réinitialise toutes les entrées du rapporteur.
"""
function réinitialiser_rapporteur()
    delete!(rapporteur_global.historique, 1:size(rapporteur_global.historique)[1])
end

"""
$(TYPEDSIGNATURES)
Ouvre un rapport enfant pour garder en mémoire le contexte donné pour de futurs avertissements.
"""
function ouvrir_rapport(contexte::AbstractString; surcontextualiser::Bool=true)
    if surcontextualiser && rapporteur_global.contexte_actuel[end] |> !isempty
        contexte = "$(rapporteur_global.contexte_actuel[end]) > $contexte"
    end
    push!(rapporteur_global.contexte_actuel, contexte)
end

"""
$(TYPEDSIGNATURES)
Ferme un rapport pour revenir au rapport parent.
"""
function fermer_rapport()
    pop!(rapporteur_global.contexte_actuel)
end

"""
$(TYPEDSIGNATURES)
Rapporte un avertissement en intégrant par défaut le contexte du rapport actuel.
# Arguments
- `message` : contenu du message ;
- `type` : type (catégorie) d’avertissement ;
- `niveau` : niveau de priorité (importance) de l’avertissement (1 étant le plus fort) ;
- `contexte` : contexte (lieu) où l’avertissement a été lancé (selon le dernier rapport ouvert).
"""
function rapporter(message::AbstractString, type::AbstractString, niveau::Int=0, contexte::AbstractString=rapporteur_global.contexte_actuel[end])
    push!(rapporteur_global.historique, [size(rapporteur_global.historique)[1] + 1, message, type, contexte, niveau])
end

"""
$(TYPEDSIGNATURES)
Génère un rapport au format *CSV*, trié selon les paramètres voulus.
"""
function générer_rapport(chemin::SystemPath=Général.configuration["système"]["chemins"]["rapport"]; tri::Union{Symbol, Vector{<:Symbol}}=[:niveau, :contexte, :chronologie])
    historique = sort(rapporteur_global.historique, tri)
    écrire(historique, chemin)
end

end
