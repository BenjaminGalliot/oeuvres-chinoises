module ŒuvresChinoises

include("général.jl")
include("rapporteur.jl")
include("structures.jl")
include("textes.jl")
include("translittérateur.jl")
include("base de données.jl")
include("extracteur.jl")
include("récupérateur.jl")
include("enrichisseur.jl")
include("générateur.jl")
include("visualisateur.jl")

using .Général
using .Rapporteur
using .Structures
using .Textes
using .Translittérateur
using .BaseDeDonnées
using .Récupérateur
using .Extracteur
using .Enrichisseur
using .Générateur
using .Visualisateur
using DocStringExtensions
using FilePaths

export préparer, répertorier_œuvres, récupérer_œuvres, enrichir_base, récupérer_tableaux, interroger, simplifier, exporter, générer_analecta, générer_rapport, visualiser

"""
$(TYPEDSIGNATURES)
Prépare le traitement en configurant tous les modules nécessaires, en préparant la journalisation et le rapporteur…
"""
function préparer()::Dict
    configuration = Général.configurer(p"configuration/configuration.yml")
    informations = obtenir_informations(p"configuration/informations.yml")
    journaliser()
    créer_rapporteur()
    Récupérateur.configurer(informations)
    BaseDeDonnées.configurer(configuration["base de données"])
    return informations
end

end
