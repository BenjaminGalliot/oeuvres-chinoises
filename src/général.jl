module Général

using CSV
using DataStructures
using DocStringExtensions
using FilePaths
using JSON
using LightXML
using Logging
using LoggingExtras
using YAML

export configurer, obtenir_informations, journaliser, lire, écrire, créer_dossier, effacer_dossier, effacer_fichier, réduire

"""
$(TYPEDSIGNATURES)
Configure toutes les données utiles à l’exécution du programme (conversions de types, surdéfinitions, etc.) sous la forme d’un dictionnaire.
"""
function configurer(chemin_configuration::SystemPath)::Dict
    informations = chemin_configuration |> lire
    conversions = Dict("chemins" => Path)
    convertir_configuration!(informations, conversions)
    global configuration = informations
end

"""
$(TYPEDSIGNATURES)
Obtient les informations utiles du fichier de configuration en convertissant si besoin certains types.
"""
function obtenir_informations(chemin_informations::SystemPath)::Dict
    informations = chemin_informations |> lire
    conversions = Dict(
        "chemins" => Path,
        "modèle" => Regex,
        "modèles" => Regex,
        "substitut" => SubstitutionString,
        "substituts" => SubstitutionString)
    convertir_configuration!(informations, conversions)
    return informations
end

"""
$(TYPEDSIGNATURES)
Configure récursivement les éléments d’intérêt (conversions de type, etc.).
"""
function convertir_configuration!(informations::AbstractDict, conversions::AbstractDict)
    for (clef, valeur) ∈ informations
        if !isnothing(informations[clef]) && haskey(conversions, clef)
            if valeur isa String
                informations[clef] = valeur |> conversions[clef]
            else
                for paire ∈ informations[clef]
                    informations[clef][paire[1]] = paire[2] .|> conversions[clef]
                end
            end
        elseif valeur isa Dict
            convertir_configuration!(valeur, conversions)
        elseif valeur isa Vector
            for élément ∈ valeur
                if élément isa Dict
                    convertir_configuration!(élément, conversions)
                end
            end
        end
    end
end

"""
$(TYPEDSIGNATURES)
Configure la journalisation en dupliquant le flux entre la console et le fichier journal.
"""
function journaliser(chemin::SystemPath=configuration["journalisation"]["chemins"]["journal"])
    enregistreur = TeeLogger(
        MinLevelLogger(FileLogger(chemin), Logging.Info),
        ConsoleLogger(stdout, Logging.Info))
    global_logger(enregistreur)
end

"""
$(TYPEDSIGNATURES)
Lit un fichier de type donné.
"""
function lire(chemin::SystemPath)::Any
    if extension(chemin) == "yml"
        return chemin |> string |> YAML.load_file
    elseif extension(chemin) == "json"
        return chemin |> string |> JSON.parsefile
    else
        return read(chemin, String)
    end
end

"""
$(TYPEDSIGNATURES)
Lit une expression au format donné.
"""
function lire(expression::AbstractString, format::AbstractString="brut")::Any
    if format == "json"
        return expression |> JSON.parse
    else
        return expression
    end
end

"""
$(TYPEDSIGNATURES)
Écrit un fichier de type donné en créant si besoin son chemin d’accès.
"""
function écrire(données::Any, chemin::SystemPath)::SystemPath
    créer_dossier(chemin)
    if extension(chemin) == "yml"
        YAML.write_file(chemin |> string, données)
    elseif extension(chemin) == "json"
        open(chemin, "w") do fichier
            JSON.print(fichier, données, 4)
        end
    elseif extension(chemin) == "csv"
        CSV.write(chemin, données; quotestrings=true, decimal=',')
    elseif extension(chemin) == "xml"
        save_file(données, chemin |> string)
    else
        write(chemin, données)
    end
    return chemin
end

"""
$(TYPEDSIGNATURES)
Crée un dossier en créant si besoin son chemin d’accès.
"""
function créer_dossier(chemin::SystemPath)
    mkpath(parent(chemin) |> string)
end

"""
$(TYPEDSIGNATURES)
Efface un dossier et tout son contenu.
"""
function effacer_dossier(chemin::SystemPath)::Nothing
    rm(chemin, force=true, recursive=true)
end

"""
$(TYPEDSIGNATURES)
Efface un fichier.
"""
function effacer_fichier(chemin::SystemPath)::Nothing
    rm(chemin, force=true)
end

"""
$(TYPEDSIGNATURES)
Réduit un vecteur à son seul contenu s’il n’est pas vide, *nothing* sinon.
"""
réduire(ensemble::AbstractArray) = isempty(ensemble) ? nothing : only(ensemble)

end
