module Enrichisseur

using ..Général
using ..Rapporteur
using ..Structures
using ..Textes
using ..BaseDeDonnées
using Chain
using DataFrames
using DataFramesMeta
using DataStructures
using DocStringExtensions
using FilePaths
using Pipe

export enrichir_base, récupérer_tableaux, interroger, simplifier

"""
$(TYPEDSIGNATURES)
Enrichit la base de données (la créant au passage) et l’enrichit à partir des œuvres récupérées et extraites.
"""
function enrichir_base(œuvres::Vector{Œuvre}; économiser::Bool=false)::Dict{String, DataFrame}
    tableaux = créer_tableaux()
    identifiants = extraire!(œuvres, tableaux)
    affilier!(œuvres, tableaux, identifiants)
    connecter_base(; économiser)
    enrichir(tableaux)
    return tableaux
end

"""
$(TYPEDSIGNATURES)
Crée la base de données en préparant les différents tableaux.
"""
function créer_tableaux()::Dict
    tableaux = Dict(
    "langues" => DataFrame(
        identifiant = Int[],
        code = String[],
        code_xml = String[],
        autoglossonyme = String[]),
    "anthroponymes" => DataFrame(
        identifiant = Int[],
        prénom = Union{String, Nothing}[],
        nom = Union{String, Nothing}[],
        prénom_transcrit = Union{String, Nothing}[],
        nom_transcrit = Union{String, Nothing}[],
        identifiant_langue = Int[]),
    "ergonymes" => DataFrame(
        identifiant = Int[],
        titre = String[],
        titre_transcrit = Union{String, Nothing}[],
        identifiant_langue = Int[]),
    "œuvres" => DataFrame(
        identifiant = Int[],
        type = String[],
        identifiant_ergonyme = Union{Int, Missing}[],
        identifiant_langue = Int[],
        source = String[],
        texte = Union{Vector{String}, Nothing}[],
        notes = Union{Vector{String}, Nothing}[],
        glossaire = Union{Vector{OrderedDict}, Nothing}[]),
    "œuvres·anthroponymes" => DataFrame(
        identifiant = Int[],
        identifiant_œuvre = Int[],
        identifiant_anthroponyme = Int[]),
    "œuvres·œuvres" => DataFrame(
        identifiant = Int[],
        identifiant_œuvre_référente = Int[],
        identifiant_œuvre_complémentaire = Int[],
        relation = String[]))
    return tableaux
end

"""
$(TYPEDSIGNATURES)
Extrait récursivement les données des œuvres pour enrichir la base de données.
"""
function extraire!(œuvres::Vector{<:Œuvre}, tableaux::Dict, identifiants::Dict=Dict{Œuvre, Int}(); est_adelphe::Bool=false)
    for œuvre ∈ œuvres
        if !haskey(identifiants, œuvre)
            if @where(tableaux["langues"], :code .== œuvre.langue.code) |> isempty
                push!(tableaux["langues"], [
                    size(tableaux["langues"], 1) + 1,
                    œuvre.langue.code,
                    œuvre.langue.code_xml,
                    œuvre.langue.autoglossonyme])
            end
            identifiant_langue = @where(tableaux["langues"], :code .== œuvre.langue.code).identifiant |> only
            if !isnothing(œuvre.titre)
                if @where(tableaux["ergonymes"], :titre .== œuvre.titre.nom) |> isempty
                    push!(tableaux["ergonymes"], [
                        size(tableaux["ergonymes"], 1) + 1,
                        œuvre.titre.nom,
                        œuvre.titre.nom_transcrit,
                        identifiant_langue])
                end
                identifiant_ergonyme =  @where(tableaux["ergonymes"], :titre .== œuvre.titre.nom).identifiant |> only
            else
                identifiant_ergonyme = missing
            end
            identifiant_œuvre = size(tableaux["œuvres"], 1) + 1
            push!(tableaux["œuvres"], [
                identifiant_œuvre,
                œuvre.type,
                identifiant_ergonyme,
                identifiant_langue,
                œuvre.source.nom,
                !isnothing(œuvre.texte) ? œuvre.texte.brut : nothing,
                !isnothing(œuvre.texte) && !isempty(œuvre.texte.notes) ? œuvre.texte.notes : nothing,
                œuvre.glossaire])
            identifiants[œuvre] = identifiant_œuvre
            for auteur ∈ œuvre.auteurs
                if @where(tableaux["anthroponymes"], :prénom .== auteur.prénom, :nom .== auteur.nom) |> isempty
                    push!(tableaux["anthroponymes"], [
                        size(tableaux["anthroponymes"], 1) + 1,
                        auteur.prénom,
                        auteur.nom,
                        auteur.prénom_transcrit,
                        auteur.nom_transcrit,
                        identifiant_langue])
                end
                identifiant_anthroponyme =  @where(tableaux["anthroponymes"], :prénom .== auteur.prénom, :nom .== auteur.nom).identifiant |> only
                push!(tableaux["œuvres·anthroponymes"], [
                    size(tableaux["œuvres·anthroponymes"], 1) + 1,
                    identifiant_œuvre,
                    identifiant_anthroponyme])
            end
            if !est_adelphe
                if !isempty(œuvre.adelphes)
                    extraire!(œuvre.adelphes, tableaux, identifiants; est_adelphe=true)
                end
            end
            extraire!(œuvre.enfants, tableaux, identifiants)
        end
    end
    return identifiants
end

"""
$(TYPEDSIGNATURES)
Interroge la base de données avec une requête.
"""
function interroger(tableaux::Dict{String, DataFrame}=BaseDeDonnées.configuration["tableaux"]; relation::AbstractString="adelphe", types::Vector{<:AbstractString}=["conte"], langues::Vector{<:AbstractString}=["fra", "zho"])::DataFrame
    résultat = @chain tableaux["œuvres"] begin
        innerjoin(tableaux["langues"], on = :identifiant_langue => :identifiant, matchmissing=:equal)
        leftjoin(tableaux["ergonymes"][:, Not(:identifiant_langue)], on = :identifiant_ergonyme => :identifiant, makeunique=true, matchmissing=:equal)
        leftjoin(tableaux["œuvres·anthroponymes"], on = :identifiant, makeunique=true, matchmissing=:equal)
        leftjoin(tableaux["anthroponymes"][:, Not(:identifiant_langue)], on = :identifiant_anthroponyme => :identifiant, makeunique=true, matchmissing=:equal)
        leftjoin(tableaux["œuvres·œuvres"], on = :identifiant => :identifiant_œuvre_référente, makeunique=true, matchmissing=:equal)
        leftjoin(tableaux["œuvres"], on = :identifiant_œuvre_complémentaire => :identifiant, makeunique=true, matchmissing=:equal, renamecols = (:_œuvre_1 => :_œuvre_2))
        leftjoin(tableaux["langues"], on = :identifiant_langue_œuvre_2 => :identifiant, makeunique=true, matchmissing=:equal, renamecols = ("" => :_œuvre_2))
        leftjoin(tableaux["ergonymes"][:, Not(:identifiant_langue)], on = :identifiant_ergonyme_œuvre_2 => :identifiant, makeunique=true, matchmissing=:equal, renamecols = ("" => :_œuvre_2))
        leftjoin(tableaux["œuvres·anthroponymes"], on = :identifiant_œuvre_complémentaire => :identifiant, makeunique=true, matchmissing=:equal, renamecols = ("" => :_œuvre_2))
        leftjoin(tableaux["anthroponymes"][:, Not(:identifiant_langue)], on = :identifiant_anthroponyme_œuvre_2 => :identifiant, makeunique=true, matchmissing=:equal, renamecols = ("" => :_œuvre_2))
        filter(ligne -> (ligne.relation_œuvre_1 |> !ismissing && ligne.relation_œuvre_1 == relation) && ligne.type_œuvre_1 ∈ types && ligne.code_œuvre_1 ∈ langues, _)
        @aside parents_œuvre_1 = trouver_parents!.(_[:, :identifiant_œuvre_1])
        @aside parents_œuvre_2 = trouver_parents!.(_[:, :identifiant_œuvre_complémentaire])
        insertcols!(:parents_œuvre_1 => parents_œuvre_1)
        insertcols!(:parents_œuvre_2 => parents_œuvre_2)
        _[:, Not(r"identifiant")]
    end
    return résultat
end

"""
$(TYPEDSIGNATURES)
Trouve la liste de tous les parents successifs d’une œuvre.
"""
function trouver_parents!(identifiant_œuvre::Int, résultats::Vector{<:NamedTuple}=NamedTuple[], tableaux::Dict{String, DataFrame}=BaseDeDonnées.configuration["tableaux"])::Vector{NamedTuple}
    identifiant_œuvre = (@where(tableaux["œuvres"], :identifiant .== identifiant_œuvre)).identifiant
    identifiant_parent = (@where(tableaux["œuvres·œuvres"], :identifiant_œuvre_référente.== identifiant_œuvre, :relation .== "parent")).identifiant_œuvre_complémentaire |> réduire
    if !isnothing(identifiant_parent)
        parent = (@where(tableaux["œuvres"], :identifiant .== identifiant_parent))
        ergonyme_parent = (@where(tableaux["ergonymes"], :identifiant .== parent.identifiant_ergonyme))
        push!(résultats, (
            titre = ergonyme_parent.titre |> only,
            type = parent.type |> only))
        trouver_parents!(identifiant_parent, résultats)
    end
    return résultats
end

"""
$(TYPEDSIGNATURES)
Affilie (parenté ou adelphie) récursivement les œuvres pour enrichir la base de données.
"""
function affilier!(œuvres::Vector{<:Œuvre}, tableaux::Dict, identifiants::Dict)
    for œuvre ∈ œuvres
        if !isnothing(œuvre.parent)
            push!(tableaux["œuvres·œuvres"], [
                size(tableaux["œuvres·œuvres"], 1) + 1,
                identifiants[œuvre],
                identifiants[œuvre.parent],
                "parent"])
        end
        if !isempty(œuvre.adelphes)
            for adelphe ∈ œuvre.adelphes
                push!(tableaux["œuvres·œuvres"], [
                    size(tableaux["œuvres·œuvres"], 1) + 1,
                    identifiants[œuvre],
                    identifiants[adelphe],
                    "adelphe"])
            end
        end
        affilier!(œuvre.enfants, tableaux, identifiants)
    end
end

"""
$(TYPEDSIGNATURES)
Simplifie le résultat d’une requête afin d’avoir rapidement les résultats intéressants.
"""
function simplifier(tableau::DataFrame)::DataFrame
    DataFrame(tableau)[:,[:titre_œuvre_1, :type_œuvre_1, :texte_œuvre_1, :titre_œuvre_2, :titre_transcrit_œuvre_2, :texte_œuvre_2]]
end

end
