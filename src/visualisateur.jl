module Visualisateur

using Blink
using DataFrames
using DocStringExtensions
using TableView

export visualiser

"""
$(TYPEDSIGNATURES)
Visualise dans une fenêtre indépendante le contenu d’un tableau.
"""
function visualiser(tableau::DataFrame)
    fenêtre = Window()
    body!(fenêtre, showtable(tableau))
end

end
