module Extracteur

using ..Rapporteur
using ..Structures
using ..Textes
using DataStructures: OrderedDict
using DocStringExtensions
using EzXML
using FilePaths
using Pipe

export extraire

"""
$(TYPEDSIGNATURES)
Extrait des données de source ctext.
"""
function extraire(œuvre::Œuvre{<:LangueAbstraite, SourceCtext}; économiser)::Dict
    données_œuvre = extraire_ctext(œuvre.données, œuvre.source.clef; économiser)
    if données_œuvre["type"] == "racine"
        résultat = Dict(
            "type" => "recueil",
            "auteur" => données_œuvre["auteur"],
            "titre" => données_œuvre["titre"],
            "contenu" => Dict[])
        position_rouleau = 0
        for identifiant_rouleau ∈ données_œuvre["contenu"]
            données_rouleau = extraire_ctext(identifiant_rouleau, œuvre.source.clef; économiser)
            if !occursin("序", données_rouleau["titre"])
                position_rouleau += 1
            end
            if occursin("*", join(vcat(données_rouleau["contenu"]...)))
                push!(résultat["contenu"], Dict(
                    "origine" => identifiant_rouleau,
                    "type" => occursin("序", données_rouleau["titre"]) ? "annexe" : "rouleau",
                    "titre" => données_rouleau["titre"],
                    "position" => occursin("序", données_rouleau["titre"]) ? nothing : position_rouleau,
                    "paragraphes bruts" => String[],
                    "contenu" => Dict[]))
                position_bloc = 0
                for bloc ∈ données_rouleau["contenu"]
                    if startswith(bloc, "*")
                        position_bloc += 1
                        push!(résultat["contenu"][end]["contenu"], Dict(
                            "origine" => identifiant_rouleau,
                            "type" => "conte",
                            "titre" => bloc[2:end],
                            "position" => position_bloc,
                            "position globale" => position_bloc + (length(filter(contenu -> haskey(contenu, "contenu"), résultat["contenu"])) ≥ 2 ? résultat["contenu"][end - 1]["contenu"][end]["position globale"] : 0),
                            "paragraphes bruts" => String[]))
                    elseif isempty(résultat["contenu"][end]["contenu"])
                        push!(résultat["contenu"][end]["paragraphes bruts"], bloc)
                    else
                        push!(résultat["contenu"][end]["contenu"][end]["paragraphes bruts"], bloc)
                    end
                end
            else
                push!(résultat["contenu"], Dict(
                    "origine" => identifiant_rouleau,
                    "type" => occursin("序", données_rouleau["titre"]) ? "annexe" : "conte",
                    "titre" => données_rouleau["titre"],
                    "position" => occursin("序", données_rouleau["titre"]) ? nothing : position_rouleau,
                    "paragraphes bruts" => données_rouleau["contenu"]))
            end
        end
    elseif données_œuvre["type"] == "branche"
        résultat = Dict(
            "type" => occursin("序", données_œuvre["titre"]) ? "annexe" : "conte",
            "titre" => données_œuvre["titre"],
            "paragraphes bruts" => données_œuvre["contenu"])
    else
        résultat = Dict()
    end
    return résultat
end

"""
$(TYPEDSIGNATURES)
Extrait des données de source Picquier.
"""
function extraire(œuvre::Œuvre{<:LangueAbstraite, SourcePicquier}; économiser)::Dict
    données_œuvre = analyser_epub(œuvre.données)
    résultat = Dict{String, Any}(
        "type" => "livre",
        "contenu" => Dict[])
    position_globale_chapitre = 0
    position_globale_conte = 0
    for fichier ∈ données_œuvre["fichiers"]
        ouvrir_rapport("$(fichier["chemin"])"; surcontextualiser=true)
        @info "Traitement du fichier « $(fichier["chemin"]) »."
        corriger!(œuvre, fichier)
        texte = fichier["contenu"] |> analyser_page
        nœuds_titre_livre = findall(œuvre.source.sélecteurs["titre de livre"], texte)
        nœuds_titre_chapitre = findall(œuvre.source.sélecteurs["titre de chapitre"], texte)
        nœuds_titre_conte = findall(œuvre.source.sélecteurs["titre de conte"], texte)
        nœuds_titre_annexe = findall(œuvre.source.sélecteurs["titre d’annexe"], texte)
        nœuds_titre_bibliographie = findall(œuvre.source.sélecteurs["bibliographie"], texte)
        nœuds_titre_glossaire = findall(œuvre.source.sélecteurs["glossaire"], texte)
        if !isempty(nœuds_titre_livre)
            nœuds_auteurs = findall(œuvre.source.sélecteurs["auteur"], texte)
            push!(résultat,
                "auteur" => nœuds_auteurs |> only |> nodecontent |> strip,
                "titre" => nœuds_titre_livre |> only |> nodecontent |> strip)
        elseif !isempty(nœuds_titre_chapitre) || !isempty(nœuds_titre_annexe)
            nom_contenu = !isempty(nœuds_titre_chapitre) ? "contenu de chapitre" : "contenu d’annexe"
            type = !isempty(nœuds_titre_chapitre) ? "chapitre" : "annexe"
            nœuds_titre = !isempty(nœuds_titre_chapitre) ? nœuds_titre_chapitre : nœuds_titre_annexe
            nœuds_paragraphe = findall(œuvre.source.sélecteurs[nom_contenu], texte)
            nœuds_note = findall(œuvre.source.sélecteurs["note"], texte)
            if !isempty(nœuds_titre_chapitre)
                position_globale_chapitre += 1
            end
            push!(résultat["contenu"], Dict(
                "origine" => fichier["chemin"],
                "type" => type,
                "titre" => nœuds_titre |> only |> nodecontent |> strip,
                "position" => !isempty(nœuds_titre_chapitre) ? position_globale_chapitre : nothing,
                "paragraphes bruts" => (@pipe nœuds_paragraphe |> filter(nœud -> nœud |> nodecontent |> strip |> !isempty, _)) .|> nodecontent .|> strip,
                "paragraphes enrichis" => (@pipe vcat(nœuds_titre, nœuds_paragraphe, nœuds_note) |> filter(nœud -> nœud |> nodecontent |> strip |> !isempty, _)),
                "notes" => (@pipe nœuds_note |> filter(nœud -> nœud |> nodecontent |> strip |> !isempty, _)) .|> nodecontent .|> strip,
                "contenu" => Dict[]))
            if !isempty(nœuds_titre_bibliographie)
                nœuds_contenu = findall(œuvre.source.sélecteurs["contenu de bibliographie"], texte)
                analyser_bibliographie!(œuvre, nœuds_contenu .|> nodecontent)
            elseif !isempty(nœuds_titre_glossaire)
                nœuds_contenu = findall(œuvre.source.sélecteurs["contenu de glossaire"], texte)
                analyser_glossaire!(œuvre, nœuds_contenu .|> nodecontent)
            end
        elseif !isempty(nœuds_titre_conte)
            éléments_conte = @pipe vcat(
                œuvre.source.sélecteurs["contenu de conte"],
                œuvre.source.sélecteurs["titre de conte"],
                œuvre.source.sélecteurs["image de conte"],
                ) |> join(_, "|")
            nœuds_élément_conte = findall(éléments_conte, texte)
            nœuds_contenu_conte = findall(œuvre.source.sélecteurs["contenu de conte"], texte)
            nœuds_titre_conte = findall(œuvre.source.sélecteurs["titre de conte"], texte)
            nœuds_image_conte = findall(œuvre.source.sélecteurs["image de conte"], texte)
            images_entreposées = []
            est_conte = true
            est_sous_conte = false
            position_conte = 0
            for nœud_élément ∈ nœuds_élément_conte
                if nœud_élément ∈ nœuds_image_conte
                    chemin_image = nœud_élément["src"] |> Path
                    @info "Image trouvée : « $chemin_image »."
                    push!(images_entreposées, chemin_image)
                elseif nœud_élément ∈ nœuds_titre_conte
                    titre = nœud_élément |> nodecontent |> strip
                    @info "Titre trouvé : « $titre »"
                    est_conte = haskey(œuvre.source.modèles, "conte") && !isnothing(match(œuvre.source.modèles["conte"], titre))
                    est_sous_conte = haskey(œuvre.source.modèles, "sous-conte") && !isnothing(match(œuvre.source.modèles["sous-conte"], titre))
                    if est_conte
                        position_globale_conte += 1
                        position_conte += 1
                        bilan = match(œuvre.source.modèles["conte"], titre)
                        if parse(Int, bilan["position"]) ≠ position_globale_conte
                            @warn "Le conte numéro « $(bilan["position"]) » se trouve à la position $position_globale_conte…"
                            rapporter("Le conte numéro « $(bilan["position"]) » se trouve à la position $position_globale_conte…", "Mauvais positionnement de conte", 1)
                        end
                        push!(résultat["contenu"][end]["contenu"], Dict(
                            "origine" => fichier["chemin"],
                            "titre" => bilan["titre"],
                            "position globale" => position_globale_conte,
                            "numéro" => position_conte,
                            "type" => "conte",
                            "paragraphes bruts" => String[],
                            "paragraphes enrichis" => [nœud_élément],
                            "images" => !isempty(images_entreposées) ? [pop!(images_entreposées)] : []))
                    elseif est_sous_conte
                        push!(get!(résultat["contenu"][end]["contenu"][end], "contenu", Dict[]), Dict(
                            "titre" => titre,
                            "type" => "sous-conte",
                            "paragraphes bruts" => String[],
                            "paragraphes enrichis" => [nœud_élément]))
                    else
                        bilan = match(œuvre.source.modèles["conte"], titre)
                        if isnothing(bilan)
                            résultat["contenu"][end]["contenu"][end]["titre"] *= " $titre"
                        else
                            error("Le conte « $titre » a un problème…")
                        end
                    end
                else
                    texte = nœud_élément |> nodecontent |> strip
                    if !isempty(texte)
                        if est_sous_conte
                            push!(résultat["contenu"][end]["contenu"][end]["contenu"][end]["paragraphes bruts"], nœud_élément |> nodecontent |> strip)
                            push!(résultat["contenu"][end]["contenu"][end]["contenu"][end]["paragraphes enrichis"], nœud_élément)
                        else
                            push!(résultat["contenu"][end]["contenu"][end]["paragraphes bruts"], nœud_élément |> nodecontent |> strip)
                            push!(résultat["contenu"][end]["contenu"][end]["paragraphes enrichis"], nœud_élément)
                        end
                    end
                end
            end
        end
        fermer_rapport()
    end
    return résultat
end

"""
$(TYPEDSIGNATURES)
Extrait des données de source inconnue…
"""
function extraire(œuvre::Œuvre{<:LangueAbstraite, <:SourceAbstraite}; économiser)::Dict
    @error "Aucun extracteur adapté pour traiter l’œuvre « $(œuvre.titre.nom) » de source « $(œuvre.source.nom) »."
    return Dict()
end


"""
$(TYPEDSIGNATURES)
Analyse la bibliographie d’une œuvre
"""
function analyser_bibliographie!(œuvre::Œuvre{<:LangueAbstraite, <:SourcePicquier}, bibliographie::Vector{<:AbstractString})
    modèle = get(œuvre.source.modèles, "référence", nothing)
    œuvre.bibliographie = OrderedDict[]
    for référence_brute ∈ bibliographie
        if (bilan = match(modèle, référence_brute)) isa RegexMatch
            référence = OrderedDict(
                "titre" => "$(bilan["article"] |> !isnothing ? (bilan["article"][end] ≠ '’' ? bilan["article"] * " " : bilan["article"]) : "")$(bilan["titre_court"])" |> strip,
                "titre lexical" => bilan["titre"] |> strip,
                "transcription" => bilan["transcription"] |> strip,
                "olographe" => bilan["olographe"] == "*",
                "tome" => bilan["tome"],
                "numéro" => (@pipe bilan["position"] |> parse(Int, _) |> string),
                "page" => (@pipe bilan["page"] |> parse(Int, _) |> string))
            if bilan["informations1766"] ≠ "–"
                push!(référence,
                    "tome de 1766" => bilan["tome1766"],
                    "numéro de 1766" => (@pipe bilan["position1766"] |> parse(Int, _) |> string))
            end
            push!(œuvre.bibliographie, référence)
        end
    end
end

"""
$(TYPEDSIGNATURES)
Analyse le glossaire d’une œuvre
"""
function analyser_glossaire!(œuvre::Œuvre{<:LangueAbstraite, <:SourcePicquier}, glossaire::Vector{<:AbstractString})
    modèle = get(œuvre.source.modèles, "glossaire", nothing)
    œuvre.glossaire = OrderedDict[]
    for note_brute ∈ glossaire
        if (bilan = match(modèle, note_brute)) isa RegexMatch
            note = OrderedDict(
                "vedette" => bilan["vedette"] |> strip,
                "définition" => bilan["explication"] |> strip,
                "contes" => @pipe bilan["positions"] |> strip |> split(_, r"[\.\s]+"; keepempty=false) |> parse.(Int, _) |> string.(_))
            push!(œuvre.glossaire, note)
        end
    end
end

"""
$(TYPEDSIGNATURES)
Corrige une œuvre source avant tout traitement à l’aide d’informations manuelles.
"""
function corriger!(œuvre::Œuvre, fichier::Any)::Any
    if !isnothing(œuvre.corrections)
        if (corrections = get(œuvre.corrections, fichier["chemin"] |> string, nothing)) |> !isnothing
            for correction ∈ corrections
                expression = fichier["contenu"]
                fichier["contenu"] = replace(fichier["contenu"], correction["modèle"] => correction["substitut"])
                if expression ≠ fichier["contenu"]
                    @warn "Correction manuelle apportée au fichier « $(fichier["chemin"]) » :\n$(correction["substitut"])"
                    rapporter(correction["substitut"], "correction manuelle", 1)
                end
            end
        end
    end
end

end
