module Récupérateur

using ..Général
using ..Rapporteur
using ..Structures
using ..Extracteur
using ..Textes
using DataStructures
using DocStringExtensions
using FilePaths
using FilePathsBase: /
using Glob
using Pipe

export répertorier_œuvres, récupérer_œuvres

global noms = Dict(
    "anthroponymes" => Dict{String, Anthroponyme}(),
    "ergonymes" => Dict{String, Ergonyme}())

"""
$(TYPEDSIGNATURES)
Configure les informations propres à ce module (liste des sources, langues et noms divers…).
"""
function configurer(données::AbstractDict)
    constructeurs = Dict(
        "sources" => Dict(
            "Philippe Picquier" => SourcePicquier,
            "ctext" => SourceCtext),
        "langues" => Dict(
            "français" => LangueFrançaise,
            "chinois" => LangueChinoise))
    global configuration = Dict(
        Dict(
            type_information => DefaultDict(
                nom -> SourceIndéfinie(nom),
                Dict(
                    nom => constructeurs[type_information][nom](;
                        Dict((@pipe clef |> replace(_, r"\s+" => "_") |> Symbol) => valeur for (clef, valeur) ∈ informations if !isnothing(valeur))...)
                    for (nom, informations) ∈ données[type_information]);
                passkey=true)
            for type_information ∈ keys(constructeurs))...,
        Dict(
            "références" => Dict{Œuvre, Dict}(),
            "œuvres" => Dict{Tuple, Œuvre}(),
            "anthroponymes" => Dict{Tuple{String, String}, Anthroponyme}(),
            "ergonymes" => Dict{String, Ergonyme}())...)
end

"""
$(TYPEDSIGNATURES)
Répertorie les œuvres qui sont renseignées dans les fichiers YAML se trouvant dans un dossier donné.
"""
function répertorier_œuvres(chemins::SystemPath=Général.configuration["œuvres"]["chemins"]["répertoires"])::Vector{Dict}
    chemins_fichiers = chemins |> string |> Glob.GlobMatch |> readdir .|> Path
    données_œuvres = Dict[]
    for (position, chemin_fichier) ∈ enumerate(chemins_fichiers)
        @info "Lecture des informations du fichier $position / $(chemins_fichiers |> length) : « $(chemin_fichier) »"
        données_œuvre = chemin_fichier |> obtenir_informations
        données_œuvre["chemin"] = chemin_fichier
        push!(données_œuvres, données_œuvre)
    end
    return données_œuvres
end

"""
$(TYPEDSIGNATURES)
Récupère des œuvres à partir du répertoire local et en utilisant dynamiquement l’extracteur adapté en fonction du type de source.
"""
function récupérer_œuvres(informations_œuvres::Array{Dict}; économiser::Bool=true)::Vector{Œuvre}
    empty!(configuration["références"])
    empty!(configuration["œuvres"])
    if !économiser
        effacer_dossier(Général.configuration["système"]["chemins"]["temporaire"])
    end
    œuvres = @pipe informations_œuvres |> préparer! |> compléter!(_; économiser)
    deviner_adelphie!()
    for (compteur_œuvre, œuvre) ∈ enumerate(œuvres)
        @info "Glossarisation automatique de l’œuvre $compteur_œuvre / $(œuvres |> length) : « $(œuvre.titre.nom) »"
        deviner_glossaire!(œuvre)
    end
    for (compteur_œuvre, œuvre) ∈ enumerate(œuvres)
        @info "Écriture de l’œuvre $compteur_œuvre / $(œuvres |> length) : « $(œuvre.titre.nom) »"
        écrire_arborescence(œuvre; économiser)
    end
    return œuvres
end

"""
$(TYPEDSIGNATURES)
Prépare les œuvres (décrites dans des fichiers YAML) qui se trouvent dans un dossier.
"""
function préparer!(informations_œuvres::Vector{Dict})::Vector{Œuvre}
    chemins = [informations_œuvre["chemin"] for informations_œuvre ∈ informations_œuvres]
    @pipe préparer!.(informations_œuvres, chemins) |> Iterators.flatten(_) |> collect
end

"""
$(TYPEDSIGNATURES)
Complète les œuvres les données d’une œuvre préparée à partir de données annexes (fichiers, sites…).
"""
function compléter!(œuvres::Vector{Œuvre}; économiser::Bool=true)::Vector{Œuvre}
    for (compteur_œuvre, œuvre) ∈ enumerate(œuvres)
        @info "Traitement de l’œuvre $compteur_œuvre / $(œuvres |> length) : « $(œuvre.titre.nom) »"
        compléter!(œuvre; économiser)
    end
    return œuvres
end

"""
$(TYPEDSIGNATURES)
Prépare récursivement les œuvres (livres, chapitres, contes, etc.) en analysant les langue, source, auteurs, traducteurs, etc., à partir des données extraites du fichier YAML, développant éventuellement certains raccourcis pour les auteurs, traducteurs, langues, etc.
"""
function préparer!(données::Dict, chemin_source::SystemPath, œuvre_parente::Union{Œuvre, Nothing}=nothing)::Vector{Œuvre}
    ouvrir_rapport(chemin_source |> string)
    œuvres = Œuvre[]
    for données_œuvre ∈ get(données, "contenu", [])
        type = données_œuvre["type"]
        langue = if (langue = get(données_œuvre, "langue", nothing)) |> !isnothing
            configuration["langues"][langue]
        elseif !isnothing(œuvre_parente)
            œuvre_parente.langue
        else
            error("Aucune langue !")
        end
        source = if (source = get(données_œuvre, "éditeur", get(données_œuvre, "site", nothing))) |> !isnothing
            configuration["sources"][source]
        elseif !isnothing(œuvre_parente)
            œuvre_parente.source
        else
            error("Aucune source !")
        end
        titre = if (titre = get(données_œuvre, "titre", nothing)) |> !isnothing
            créer_ergonyme(titre, langue)
        end
        @info "Traitement du fichier « $(chemin_source |> basename) » ($type), intitulé « $(!isnothing(titre) ? titre.nom : "?") »$(!isnothing(œuvre_parente) ? ", œuvre parente : « $(œuvre_parente.titre.nom) »." : ".")"
        auteurs = if (auteurs = get(données_œuvre, "auteur", get(données_œuvre, "traducteur", nothing))) |> !isnothing
            créer_anthroponymes(auteurs, langue)
        elseif haskey(données_œuvre, "contenu")
            @pipe (regrouper_champs(données_œuvre["contenu"], ["auteur", "traducteur"], données_œuvre["langue"])) |> créer_anthroponymes(_, langue)
        elseif !isnothing(œuvre_parente) && !isempty(œuvre_parente.auteurs)
            œuvre_parente.auteurs
        else
            Anthroponyme{typeof(langue)}[]
        end
        renseignement = chemin_source
        chemin = isnothing(œuvre_parente) ? parent(chemin_source) / titre.nom : œuvre_parente.chemin / titre.nom
        données = haskey(données_œuvre, "fichiers") ? (@pipe données_œuvre["fichiers"] .|> joinpath(chemin_source |> parent, _)) : get(données_œuvre, "identifiant", nothing)
        corrections = haskey(données_œuvre, "corrections") ? données_œuvre["corrections"] |> préparer_corrections : nothing
        œuvre = créer_œuvre(œuvre_parente; type, titre, langue, auteurs, source, renseignement, chemin, données, corrections)
        if (référence = get(données_œuvre, "référence", nothing)) |> !isnothing
            if !isnothing(œuvre_parente) && (référence_parente = get(configuration["références"], œuvre_parente, nothing)) |> !isnothing
                référence = merge(référence_parente, référence)
            end
            configuration["références"][œuvre] = référence
        end
        push!(œuvres, œuvre)
        préparer!(données_œuvre, chemin_source, œuvre)
    end
    fermer_rapport()
    return œuvres
end

"""
$(TYPEDSIGNATURES)
Complète récursivement les données d’une œuvre préparée (et les sous-œuvres éventuelles) à partir de données annexes (fichiers, sites…).
"""
function compléter!(œuvre::Œuvre, données::Union{Dict, Nothing}=nothing; économiser=true)::Œuvre
    ouvrir_rapport(!isnothing(œuvre.titre) ? œuvre.titre.nom : "nom inconnu")
    données = !isnothing(œuvre.données) ? extraire(œuvre; économiser) : données
    titres_enfants = Dict(enfant.titre.nom => position for (position, enfant) ∈ enumerate(œuvre.enfants) if !isnothing(enfant.titre))
    données_enfants = !isnothing(données) ? get(données, "contenu", []) : [extraire(enfant; économiser) for enfant ∈ œuvre.enfants if !isnothing(enfant.données)]
    if !isempty(données_enfants)
        for (position_enfant, données_enfant) ∈ enumerate(données_enfants)
            langue = œuvre.langue
            source = œuvre.source
            position_œuvre = get(titres_enfants, données_enfant["titre"], nothing)
            numéro_œuvre = (numéro = get(données_enfant, "position", get(données_enfant, "numéro", nothing))) |> !isnothing ? numéro |> string : nothing
            numéro_global_œuvre = (numéro = get(données_enfant, "position globale", nothing)) |> !isnothing ? numéro |> string : nothing
            brut = get(données_enfant, "paragraphes bruts", [])
            notes = get(données_enfant, "notes", [])
            enrichi = get(données_enfant, "paragraphes enrichis", [])
            texte = if !isempty(brut)
                Texte(; brut, notes, enrichi, langue, source) |> nettoyer!
            end
            if !isnothing(position_œuvre)
                œuvre_enfant = popat!(œuvre.enfants, position_œuvre)
                œuvre_enfant.texte = texte
                œuvre_enfant.numéro = numéro_œuvre
                œuvre_enfant.numéro_global = numéro_global_œuvre
                apparenter!(œuvre_enfant, œuvre)
            else
                titre = if !isempty(données_enfant["titre"])
                    créer_ergonyme(données_enfant["titre"], langue) |> nettoyer!
                end
                type = données_enfant["type"]
                auteurs = œuvre.auteurs
                renseignement = œuvre.renseignement
                origine = get(données_enfant, "origine", nothing)
                chemin = splitext(œuvre.chemin)[1] / données_enfant["titre"] * splitext(œuvre.chemin)[2]
                numéro = numéro_œuvre
                numéro_global = numéro_global_œuvre
                images = get(données_enfant, "images", [])
                numéro_global = numéro_global_œuvre
                œuvre_enfant = créer_œuvre(œuvre; type, titre, langue, auteurs, source, renseignement, origine, chemin, numéro, numéro_global, texte, images)
            end
        end
    end
    compléter!.(œuvre.enfants, !isempty(données_enfants) ? données_enfants : nothing)
    fermer_rapport()
    return œuvre
end

"""
$(TYPEDSIGNATURES)
Devine les adelphies d’œuvres extraites à partir de tous les indices possibles (titres, positions, comptages congruents, etc.).
"""
function deviner_adelphie!(références::Dict{Œuvre, Dict}=configuration["références"])::Dict{Œuvre, Dict}
    for (compteur_œuvre, (œuvre, informations)) ∈ enumerate(références)
        ouvrir_rapport(@pipe [œuvre.titre.nom for œuvre ∈ récupérer_parents!(œuvre)] |> reverse |> join(_, " > "))
        @info "Adelphisation automatique de l’œuvre $compteur_œuvre / $(références |> length) : « $(œuvre.titre.nom) »"
        adelphe = chercher_adelphe(informations)
        if !isnothing(adelphe)
            adelphiser!([œuvre, adelphe])
            if (titre = get(informations, "titre", nothing)) |> !isnothing
                créer_ergonyme(titre, configuration["langues"][informations["langue"]])
            end
        end
        if !isnothing(œuvre.données)
            deviner_adelphie!(œuvre)
            deviner_transcription!(œuvre)
        end
        fermer_rapport()
    end
    return références
end

"""
$(TYPEDSIGNATURES)
Devine une adelphie d’œuvre extraite à partir de comptages congruents.
"""
function deviner_adelphie!(œuvre::Œuvre, niveaux::Vector{<:AbstractString}=["chapitre", "conte"], niveau_actuel::Int=1)::Œuvre
    adelphie = vcat(œuvre, œuvre.adelphes)
    œuvres_alignées = [[enfant for enfant ∈ œuvre.enfants if enfant.type ∈ ["chapitre", "rouleau", "conte"]] for œuvre ∈ adelphie]
    comptage_œuvres = œuvres_alignées .|> length
    if comptage_œuvres |> Set |> length > 1
        @warn "L’adelphisation de l’œuvre « $(œuvre.titre.nom) » ne semble pas se baser sur un alignement congruent des chapitres/rouleaux ($(join(comptage_œuvres, ", ")))…"
        rapporter(œuvre.titre.nom, "adelphisation incongruente", 3)
        taille_limite = max(comptage_œuvres...)
        for (position, suite_œuvres) ∈ œuvres_alignées |> enumerate
            œuvres_alignées[position] = vcat(suite_œuvres, repeat([nothing], taille_limite - length(suite_œuvres)))
        end
    end
    for enfants_adelphes ∈ zip(œuvres_alignées...) .|> collect
        enfants_adelphes = @pipe enfants_adelphes |> filter(!isnothing, _) |> Vector{Œuvre}
        adelphiser!(enfants_adelphes)
        if niveau_actuel < length(niveaux)
            deviner_adelphie!.(enfants_adelphes, [niveaux], niveau_actuel + 1)
        end
    end
    return œuvre
end

"""
$(TYPEDSIGNATURES)
Cherche une œuvre adelphe selon différents critères (titres, positions, etc.).
"""
function chercher_adelphe(informations::Dict)::Union{Œuvre, Nothing}
    identifiant = [(@pipe informations[niveau] |> analyser_ergonyme(_, configuration["langues"][informations["langue"]])).nom for niveau ∈ ["œuvre", "titre"] if get(informations, niveau, nothing) |> !isnothing]
    positionnement = [informations[niveau] |> string for niveau ∈ ["rouleau", "conte"] if haskey(informations, niveau)]
    adelphe = get(configuration["œuvres"], identifiant |> Tuple, nothing)
    if isnothing(adelphe) && haskey(informations, "œuvre") && !isempty(positionnement)
        adelphe = get(configuration["œuvres"], vcat(informations["œuvre"], positionnement) |> Tuple, nothing)
    end
    if isnothing(adelphe)
        œuvres_potentielles = filter(élément -> élément.first[[1, end]] == identifiant |> Tuple, configuration["œuvres"])
        adelphe = if (nombre = length(œuvres_potentielles)) == 1
            (œuvres_potentielles |> only).second
        elseif nombre > 1
            adelphes = @pipe [œuvre.second.origine for œuvre ∈ œuvres_potentielles] |> join(_, ", ")
            @warn "Plusieurs œuvres adelphes trouvées avec ce titre : $adelphes."
            rapporter("$(informations["titre"]) : $adelphes", "multiples œuvres adelphes", 2)
            nothing
        else
            @warn "Aucune œuvre adelphe trouvée."
            rapporter(informations["titre"], "aucune œuvre adelphe", 3)
            nothing
        end
    end
    return adelphe
end

"""
$(TYPEDSIGNATURES)
Devine le glossaire (notes) d’une œuvre extraite à partir du glossaire général.
"""
function deviner_glossaire!(œuvre::Œuvre, glossaire::Union{AbstractDict, Nothing}=nothing)::Œuvre
    ouvrir_rapport(œuvre.titre.nom)
    glossaire = !isnothing(glossaire) ? glossaire : (!isnothing(œuvre.glossaire) ? restructurer_glossaire(œuvre.glossaire) : nothing)
    if !isnothing(glossaire)
        notes = get(glossaire, œuvre.numéro, nothing)
        if !isnothing(notes) && œuvre.type == "conte"
            œuvre.glossaire = notes
        end
        for enfant ∈ œuvre.enfants
            deviner_glossaire!(enfant, glossaire)
        end
    end
    fermer_rapport()
    return œuvre
end

"""
$(TYPEDSIGNATURES)
Devine la transcription d’une œuvre extraite à partir des bibliographies des traductions.
"""
function deviner_transcription!(œuvre::Œuvre, bibliographie::Union{Vector, Nothing}=nothing)::Œuvre
    bibliographie = !isnothing(bibliographie) ? bibliographie : œuvre.bibliographie
    if !isnothing(bibliographie)
        transcription = déterminer_transcription(œuvre, bibliographie)
        if !isnothing(transcription)
            for adelphe ∈ œuvre.adelphes
                if adelphe.titre.nom_transcrit ∉ [nothing, transcription]
                    @warn "La transcription « $transcription » remplace « $(adelphe.titre.nom_transcrit) »."
                    rapporter("$(adelphe.titre.nom) ($transcription → $(adelphe.titre.nom_transcrit))", "remplacement de transcription", 2)
                end
                adelphe.titre.nom_transcrit = transcription
            end
        end
        for enfant ∈ œuvre.enfants
            deviner_transcription!(enfant, bibliographie)
        end
    end
    return œuvre
end

"""
$(TYPEDSIGNATURES)
Apparente deux œuvres en les liant par les champs adéquats.
"""
function apparenter!(œuvre::Œuvre, œuvre_parente::Œuvre)::Œuvre
    œuvre.parent = œuvre_parente
    push!(œuvre_parente.enfants, œuvre)
    return œuvre
end

"""
$(TYPEDSIGNATURES)
Adelphise deux œuvres en les liant par les champs adéquats.
"""
function adelphiser!(œuvres_adelphes::Vector{Œuvre})::Vector{Œuvre}
    for œuvre_référente ∈ œuvres_adelphes
        append!(œuvre_référente.adelphes, [œuvre for œuvre ∈ œuvres_adelphes if œuvre ≠ œuvre_référente && œuvre ∉ œuvre_référente.adelphes])
        sort!(œuvre_référente.adelphes; by = œuvre -> œuvre.titre.nom)
    end
    return œuvres_adelphes
end

"""
$(TYPEDSIGNATURES)
Restructure un glossaire pour faciliter les traitements.
"""
function restructurer_glossaire(glossaire::Vector{<:AbstractDict})::Dict
    structure = Dict{String, Vector{OrderedDict}}()
    for note ∈ glossaire
        numéros = get(note, "contes", [])
        for numéro ∈ numéros
            if !haskey(structure, numéro)
                structure[numéro] = OrderedDict[]
            end
            push!(structure[numéro], OrderedDict(clef => valeur for (clef, valeur) ∈ note if clef ≠ "contes"))
        end
    end
    return structure
end

"""
$(TYPEDSIGNATURES)
Détermine la bonne transcription d’une œuvre à partir de la bibliographie et de sa position.
"""
function déterminer_transcription(œuvre::Œuvre, bibliographie::Vector)::Union{String, Nothing}
    résultats = findall(référence -> œuvre.titre.nom == référence["titre"], bibliographie)
    résultat = if length(résultats) == 1
        @pipe résultats |> only |> bibliographie[_]["transcription"] |> lowercase
    elseif length(résultats) ≥ 2
        [bibliographie[résultat]["transcription"] for résultat ∈ résultats if bibliographie[résultat]["numéro"] == œuvre.numéro_global] |> only |> lowercase
    else
        résultats = findall(référence -> occursin(référence["titre"], œuvre.titre.nom), bibliographie)
        résultat = if length(résultats) == 1
            @pipe résultats |> only |> bibliographie[_]["transcription"] |> lowercase
        elseif length(résultats) ≥ 2
            @warn "Ambiguïté secondaire dans la détermination de la transcription pour l’œuvre « $(œuvre.titre.nom) »."
            rapporter(œuvre.titre.nom, "ambiguïté secondaire de détermination de transcription", 3)
        else
            nothing
        end
    end
end

"""
$(TYPEDSIGNATURES)
Regroupe les champs de même nom dans les données d’une œuvre (typiquement des champs d’anthroponymes aux différents niveaux).
"""
function regrouper_champs(données::Vector{<:Dict}, intitulés::Vector{<:AbstractString}, langue::AbstractString)::Any
    valeurs = []
    for données_œuvre ∈ données
        for intitulé ∈ intitulés
            if (valeur = get(données_œuvre, intitulé, nothing)) |> !isnothing
                push!(valeurs, valeur)
            end
        end
    end
    valeurs = @pipe valeurs |> Set |> join(_, ", ")
    return valeurs
end

"""
$(TYPEDSIGNATURES)
Crée une œuvre en la mettant éventuellement au passage dans la liste ad hoc pour une potentielle référence.
"""
function créer_œuvre(œuvre_parente::Union{Œuvre, Nothing}=nothing; membres...)::Œuvre
    œuvre = Œuvre(; membres...)
    if !isnothing(œuvre_parente)
        apparenter!(œuvre, œuvre_parente)
    end
    if !isnothing(œuvre.titre)
        identifiant = créer_identifiant_ergonymique!(œuvre) |> Tuple
        configuration["œuvres"][identifiant] = œuvre
        if !isnothing(œuvre.numéro)
            identifiant = créer_identifiant_positionnel!(œuvre) |> Tuple
            configuration["œuvres"][identifiant] = œuvre
        end
    end
    return œuvre
end

"""
$(TYPEDSIGNATURES)
Crée un ergonyme en le mettant au passage dans la liste ad hoc pour une potentielle future occurrence.
"""
function créer_ergonyme(expression::AbstractString, langue::LangueAbstraite)::Ergonyme{typeof(langue)}
    titre = analyser_ergonyme(expression, langue)
    ergonyme = get!(configuration["ergonymes"], titre.nom, Ergonyme(titre.nom, titre.transcription, langue))
    if isnothing(ergonyme.nom_transcrit) && titre.transcription ≠ ergonyme.nom_transcrit
        ergonyme.nom_transcrit = titre.transcription
    end
    return ergonyme
end

"""
$(TYPEDSIGNATURES)
Crée une liste d’anthroponymes à partir d’une expression quelconque comprenant éventuellement des séparateurs.
"""
function créer_anthroponymes(expression::AbstractString, langue::LangueAbstraite; intraséparateur::Regex=r"\s+", interséparateur::Regex=r",\s")::Vector{Anthroponyme{typeof(langue)}}
    anthroponymes = @pipe expression |> split(_, interséparateur; keepempty=false)
    créer_anthroponyme.(anthroponymes, langue)
end

"""
$(TYPEDSIGNATURES)
Crée un anthroponyme en le mettant au passage dans la liste ad hoc pour une potentielle future occurrence.
"""
function créer_anthroponyme(expression::AbstractString, langue::LangueAbstraite)::Anthroponyme{typeof(langue)}
    anthroponyme = analyser_anthroponyme(expression, langue)
    (prénom, nom) = split(anthroponyme.nom)
    (prénom_transcrit, nom_transcrit) = !isnothing(anthroponyme.transcription) ? split(anthroponyme.transcription) : (nothing, nothing)
    get!(configuration["anthroponymes"], (prénom, nom), Anthroponyme(; prénom, nom, prénom_transcrit, nom_transcrit, langue))
end

"""
$(TYPEDSIGNATURES)
Crée un anthroponyme en le mettant au passage dans la liste ad hoc pour une potentielle future occurrence.
"""
function créer_anthroponyme(expression::AbstractString, langue::LangueChinoise)::Anthroponyme{typeof(langue)}
    anthroponyme = analyser_anthroponyme(expression, langue)
    (nom, prénom) = !isnothing(anthroponyme.nom) ? split(anthroponyme.nom) : (nothing, nothing)
    (nom_transcrit, prénom_transcrit) = !isnothing(anthroponyme.transcription) ? split(anthroponyme.transcription) : (nothing, nothing)
    get!(configuration["anthroponymes"], (something(prénom, prénom_transcrit), something(nom, nom_transcrit)), Anthroponyme(; prénom, nom, prénom_transcrit, nom_transcrit, langue))
end

"""
$(TYPEDSIGNATURES)
Analyse un ergonyme issu des données des fichiers YAML.
"""
function analyser_ergonyme(nom::AbstractString, langue::LangueAbstraite)::Union{NamedTuple, Nothing}
    bilan = match(langue.modèles["ergonyme"], nom)
    groupes = Base.PCRE.capture_names(langue.modèles["ergonyme"].regex) |> values
    résultat = if !isnothing(bilan)
        (
            nom = !isnothing(bilan["nom"]) ? bilan["nom"] |> strip : nothing,
            transcription = "transcription" ∈ groupes ? !isnothing(bilan["transcription"]) ? bilan["transcription"] |> strip : nothing : nothing)
    else
        @warn "Aucun ergonyme trouvé pour « $nom »."
        rapporter(nom, "aucun ergonyme trouvé", 3)
    end
end

"""
$(TYPEDSIGNATURES)
Analyse un anthroponyme issu des données des fichiers YAML.
"""
function analyser_anthroponyme(nom::AbstractString, langue::LangueAbstraite)::Union{NamedTuple, Nothing}
    bilan = match(langue.modèles["anthroponyme"], nom)
    groupes = Base.PCRE.capture_names(langue.modèles["anthroponyme"].regex) |> values
    résultat = if !isnothing(bilan)
        (
            nom = !isnothing(bilan["nom"]) ? bilan["nom"] |> strip : nothing,
            transcription = "transcription" ∈ groupes ? !isnothing(bilan["transcription"]) ? bilan["transcription"] |> strip : nothing : nothing)
    else
        @warn "Aucun anthroponyme trouvé pour « $nom »."
        rapporter(nom, "aucun anthroponyme trouvé", 3)
    end
end

"""
$(TYPEDSIGNATURES)
Écrit l’arborescence d’une œuvre (avec chapitres et contes) dans un fichier d’index.
"""
function écrire_arborescence(œuvre::Œuvre, chemin::SystemPath=Général.configuration["œuvres"]["chemins"]["cible"], nom_index::SystemPath=Général.configuration["œuvres"]["chemins"]["index"]; économiser::Bool=true)::SystemPath
    ouvrir_rapport(œuvre.titre.nom)
    cible = chemin / œuvre.titre.nom
    if !économiser && isdir(cible)
        effacer_dossier(cible)
    end
    chemin_fichier = cible / nom_index
    écrire(œuvre, chemin_fichier)
    @info "Écriture du fichier d’index « $chemin_fichier »."
    fermer_rapport()
    return chemin_fichier
end

"""
$(TYPEDSIGNATURES)
Prépare les corrections d’une œuvre source (avant tout traitement) à l’aide d’informations manuelles et structurées pour faciliter les traitements.
"""
function préparer_corrections(données::Vector{<:AbstractDict})::Dict
    corrections = Dict()
    for correction ∈ données
        if !haskey(corrections, correction["lieu"])
            corrections[correction["lieu"]] = Dict[]
        end
        push!(corrections[correction["lieu"]], Dict(paire for paire ∈ correction if paire[1] ≠ "lieu"))
    end
    return corrections
end

"""
$(TYPEDSIGNATURES)
Récupère récursivement tous les parents d’une œuvre.
"""
function récupérer_parents!(œuvre::Œuvre, parents::Vector{<:Œuvre}=Œuvre[])::Vector{Œuvre}
    push!(parents, œuvre)
    if !isnothing(œuvre.parent)
        récupérer_parents!(œuvre.parent, parents)
    end
    return parents
end

"""
$(TYPEDSIGNATURES)
Crée récursivement l’identifiant ergonymique arborescent d’une œuvre.
"""
function créer_identifiant_ergonymique!(œuvre::Œuvre, identifiant::Vector{<:AbstractString}=String[])::Vector{String}
    push!(identifiant, œuvre.titre.nom)
    if !isnothing(œuvre.parent)
        créer_identifiant_ergonymique!(œuvre.parent, identifiant)
    end
    return identifiant |> reverse
end

"""
$(TYPEDSIGNATURES)
Crée récursivement l’identifiant positionnel arborescent d’une œuvre.
"""
function créer_identifiant_positionnel!(œuvre::Œuvre, identifiant::Vector{<:AbstractString}=String[])::Vector{String}
    if !isnothing(œuvre.numéro)
        push!(identifiant, œuvre.numéro)
    end
    if !isnothing(œuvre.parent)
        créer_identifiant_positionnel!(œuvre.parent, identifiant)
    else
        push!(identifiant, œuvre.titre.nom)
    end
    return identifiant |> reverse
end

end
