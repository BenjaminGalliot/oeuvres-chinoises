module BaseDeDonnées

using ..Général
using DataFrames
using DBInterface
using DocStringExtensions
using FilePaths
using Pipe
using SQLite

export connecter_base, créer_base_de_données, enrichir, récupérer_tableaux

Base.broadcastable(structure::T) where T <: Union{SQLite.DB} = Ref(structure)

"""
$(TYPEDSIGNATURES)
Configure les informations propres à ce module (chemins d’accès, connexion, paramètres divers…).
"""
function configurer(informations::AbstractDict)
    global configuration = informations
end

"""
$(TYPEDSIGNATURES)
Se connecte à une base de données existante (PostgreSQL ou SQLite) et la crée si besoin.
"""
function connecter_base(paramètres::AbstractDict=configuration["paramètres"]; économiser::Bool=true)
    if !économiser
        effacer_fichier(paramètres["chemins"]["base"])
    end
    if isnothing(get(configuration, "connexion", nothing)) || !économiser
        configuration["connexion"] = SQLite.DB(paramètres["chemins"]["base"] |> string)
        DBInterface.execute(configuration["connexion"], "PRAGMA foreign_keys = ON")
        SQLite.@register configuration["connexion"] SQLite.regexp
    end
    return configuration["connexion"]
end

"""
$(TYPEDSIGNATURES)
Enrichit la base de données.
"""
function enrichir(tableaux::Dict{<:AbstractString, DataFrame})
    for tableau ∈ tableaux |> values, colonne ∈ 1:size(tableau, 2)
        tableau[!,colonne] = replace(tableau[:,colonne], nothing => missing)
    end
    SQLite.load!.(tableaux |> values, configuration["connexion"], tableaux |> keys)
end

"""
$(TYPEDSIGNATURES)
Récupère les tableaux d’intérêt de la base de données.
"""
function récupérer_tableaux()::Dict{String, DataFrame}
    connecter_base()
    requête_générale = "SELECT name AS nom FROM sqlite_master WHERE type = 'table' AND name NOT LIKE 'sqlite_%';"
    nom_tableaux = DBInterface.execute(configuration["connexion"], requête_générale) |> DataFrame
    requêtes_spécifiques = nom_tableaux.nom .|> tableau -> (tableau, "SELECT * FROM $tableau")
    configuration["tableaux"] = Dict(nom => DBInterface.execute(configuration["connexion"], requête) |> DataFrame for (nom, requête) ∈ requêtes_spécifiques)
end

end