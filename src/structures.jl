module Structures

using DataStructures: OrderedDict
using DocStringExtensions
using FilePaths
using Glob
using Parameters
using Pipe
using YAML

export LangueFrançaise, LangueChinoise, SourceCtext, SourcePicquier, SourceIndéfinie, Anthroponyme, Ergonyme, EntitéArborescente, Œuvre, Dyade, Texte
export EntitéArborescente, LangueAbstraite, SourceAbstraite, NomAbstrait
export configurer

abstract type LangueAbstraite end
abstract type SourceAbstraite end
abstract type NomAbstrait{Langue <: LangueAbstraite} end
abstract type EntitéArborescente{Langue <: LangueAbstraite, Source <: SourceAbstraite} end

@with_kw struct LangueFrançaise <: LangueAbstraite
    code::String = "fra"
    code_xml::String = "fr"
    autoglossonyme::String = "français"
    modèles::Dict{String, Regex} = Dict()
    substituts::Dict{String, Union{String, SubstitutionString}} = Dict()
    affichages::Dict{String, Int} = Dict()
    niveaux::Dict{String, Int} = Dict()
end

@with_kw struct LangueChinoise <: LangueAbstraite
    code::String = "zho"
    code_xml::String = "zh"
    autoglossonyme::String = "中文"
    modèles::Dict{String, Regex} = Dict()
    substituts::Dict{String, Union{String, SubstitutionString}} = Dict()
    affichages::Dict{String, Int} = Dict()
    niveaux::Dict{String, Int} = Dict()
end

@with_kw struct SourceCtext <: SourceAbstraite
    nom::String = "ctext"
    clef::String
    modèles::Dict{String, Regex} = Dict()
end

@with_kw struct SourcePicquier <: SourceAbstraite
    nom::String = "Picquier"
    sélecteurs::Dict{String, Any} = Dict()
    modèles::Dict{String, Regex} = Dict()
    substituts::Dict{String, Union{String, SubstitutionString}} = Dict()
    affichages::Dict{String, Int} = Dict()
    niveaux::Dict{String, Int} = Dict()
end

@with_kw struct SourceIndéfinie <: SourceAbstraite
    nom::String
end

@with_kw mutable struct Anthroponyme{Langue <: LangueAbstraite} <: NomAbstrait{Langue}
    prénom::Union{String, Nothing} = nothing
    nom::Union{String, Nothing} = nothing
    prénom_transcrit::Union{String, Nothing} = nothing
    nom_transcrit::Union{String, Nothing} = nothing
    langue::Langue
end

@with_kw mutable struct Ergonyme{Langue <: LangueAbstraite} <: NomAbstrait{Langue}
    nom::String
    nom_transcrit::Union{String, Nothing} = nothing
    langue::Langue
end

@with_kw mutable struct Texte{Langue<:LangueAbstraite, Source<:SourceAbstraite}
    brut::Vector{String} = []
    notes::Vector{String} = []
    enrichi::Vector = []
    langue::Langue
    source::Source
end

@with_kw mutable struct Œuvre{Langue <: LangueAbstraite, Source <: SourceAbstraite} <: EntitéArborescente{Langue, Source}
    type::AbstractString = "livre"
    titre::Union{Ergonyme{Langue}, Nothing} = nothing
    numéro::Union{String, Nothing} = nothing
    numéro_global::Union{String, Nothing} = nothing
    langue::Langue
    source::Source
    auteurs::Vector{Anthroponyme{Langue}} = Anthroponyme{typeof(langue)}[]
    renseignement::String
    origine::Union{String, Nothing} = nothing
    chemin::Union{SystemPath, Nothing} = nothing
    texte::Union{Texte{Langue, Source}, Nothing} = nothing
    images::Vector{SystemPath} = []
    données::Union{T, Vector{T}, Nothing} where T <: Union{String, SystemPath, Dict} = nothing
    parent::Union{Œuvre{Langue, Source}, Nothing} = nothing
    enfants::Vector{Œuvre{Langue, Source}} = Œuvre{typeof(langue), typeof(source)}[]
    adelphes::Vector{Œuvre} = Œuvre[]
    glossaire::Union{Vector{OrderedDict}, Nothing} = nothing
    bibliographie::Union{Vector{OrderedDict}, Nothing} = nothing
    corrections::Union{Dict, Nothing} = nothing
end

StructuresLinguistiques = Union{<:LangueAbstraite, <:SourceAbstraite, <:NomAbstrait, <:Texte, <:EntitéArborescente}

Base.broadcastable(structure::T) where T <: StructuresLinguistiques = Ref(structure)

Dict(structure::T) where T <: StructuresLinguistiques = Dict(clef => getfield(structure, clef) for clef ∈ fieldnames(T))

OrderedDict(structure::T) where T <: StructuresLinguistiques = OrderedDict(clef => getfield(structure, clef) for clef ∈ fieldnames(T))

YAML._print(interface::IO, lien::SystemPath, niveau::Int=0, niveau_ignoré::Bool=false) = YAML._print(interface, lien |> string)

YAML._print(interface::IO, langue::LangueAbstraite, niveau::Int=0, niveau_ignoré::Bool=false) = YAML._print(interface, langue.autoglossonyme)

YAML._print(interface::IO, source::SourceAbstraite, niveau::Int=0, niveau_ignoré::Bool=false) = YAML._print(interface, source.nom)

function YAML._print(interface::IO, ergonyme::Ergonyme{LangueChinoise}, niveau::Int=0, niveau_ignoré::Bool=false)
    nom_transcrit = !isnothing(ergonyme.nom_transcrit) ? " ($(ergonyme.nom_transcrit))" : ""
    YAML._print(interface, "$(ergonyme.nom)$nom_transcrit")
end

YAML._print(interface::IO, ergonyme::Ergonyme{LangueFrançaise}, niveau::Int=0, niveau_ignoré::Bool=false) = YAML._print(interface, ergonyme.nom)

YAML._print(interface::IO, anthroponyme::Anthroponyme{LangueFrançaise}, niveau::Int=0, niveau_ignoré::Bool=false) = YAML._print(interface, "$(anthroponyme.prénom) $(anthroponyme.nom)" |> String)

function YAML._print(interface::IO, anthroponyme::Anthroponyme{LangueChinoise}, niveau::Int=0, niveau_ignoré::Bool=false)
    nom = !isnothing(anthroponyme.nom) ? "$(anthroponyme.nom)$(anthroponyme.prénom)" : nothing
    transcription = !isnothing(anthroponyme.nom_transcrit) ? "($(anthroponyme.nom_transcrit) $(anthroponyme.prénom_transcrit))" : nothing
    YAML._print(interface, @pipe [nom, transcription] |> filter(!isnothing, _) |> join(_, " "))
end

function YAML._print(interface::IO, texte::Texte, niveau::Int=0, niveau_ignoré::Bool=false)
    YAML.print(interface, YAML._indent("\n", niveau))
    for champ ∈ [:brut, :notes, :enrichi]
        valeur = getfield(texte, champ)
        if !(valeur isa AbstractVector && isempty(valeur)) && !isnothing(valeur)
            YAML._print(interface, Dict(champ |> String => getfield(texte, champ)), niveau)
        end
    end
end

function YAML._print(interface::IO, œuvre::Œuvre, niveau::Int=0, niveau_ignoré::Bool=false)
    for (position, (clef, valeur)) ∈ œuvre |> OrderedDict |> enumerate
        if !(valeur isa AbstractVector && isempty(valeur)) && !isnothing(valeur)
            clef = @pipe clef |> String |> replace(_, "_" => " ")
            clefs_affichables = niveau == 0 ? ("type", "titre", "numéro", "numéro_global", "langue", "source", "auteurs", "enfants", "chemin", "texte", "images", "renseignement", "origine", "glossaire", "bibliographie") : ("type", "titre", "numéro", "numéro global", "auteurs", "enfants", "chemin", "texte", "images", "origine", "glossaire")
            if clef ∈ clefs_affichables
                YAML._print(interface, (clef => valeur), niveau, position == 1)
            elseif clef == "adelphes"
                YAML._print(interface, (clef => [adelphe.titre for adelphe ∈ valeur]), niveau, position == 1)
            end
        end
    end
end

# Base.show(interface::IO, langue::LangueAbstraite) = print(interface, "$(typeof(langue))")
# Base.show(interface::IO, source::SourceAbstraite) = print(interface, "$(typeof(source))")
# Base.show(interface::IO, anthroponyme::Anthroponyme) = print(interface, "Anthroponyme⧼$(anthroponyme.prénom) $(!isnothing(anthroponyme.prénom_transcrit) ? anthroponyme.prénom_transcrit : "") $(anthroponyme.nom) $(!isnothing(anthroponyme.nom_transcrit) ? anthroponyme.nom_transcrit : "") ($(anthroponyme.langue.code))⧽")
# Base.show(interface::IO, ergonyme::Ergonyme) = print(interface, "Ergonyme⧼$(ergonyme.nom) $(!isnothing(ergonyme.nom_transcrit) ? ergonyme.nom_transcrit : "") ($(ergonyme.langue.code))⧽")
# Base.show(interface::IO, œuvre::Œuvre) = print(interface, "Œuvre⧼$(œuvre.type) $(!isnothing(œuvre.titre) ? œuvre.titre.nom : "?"))⧽")

end
