using Dates
using FilePaths
using FilePathsBase: /
using Pkg
using Documenter


push!(LOAD_PATH, cwd())
using ŒuvresChinoises

function créer_documentation(dossier_cible::SystemPath)
    (dossier_source, chemin_index, pages_documentation) = créer_fichiers_documentation()
    créer_site(dossier_source, dossier_cible, chemin_index, pages_documentation)
end

function créer_site(dossier_source::SystemPath, dossier_cible::SystemPath, chemin_index::SystemPath, pages_documentation::AbstractVector)
    branche = get(ENV, "CI_DEFAULT_BRANCH", "maîtresse")
    documentation = makedocs(
        sitename = "Œuvres chinoises",
        format = Documenter.HTML(
            prettyurls = haskey(ENV, "GITLAB_USER_ID"),
            assets = []),
        modules = [ŒuvresChinoises],
        pages = [
            "Accueil" => chemin_index |> string,
            "Documentation" => pages_documentation],
        source = dossier_source |> string,
        build = dossier_cible |> string,
        repo = "https://gitlab.com/BenjaminGalliot/oeuvres-chinoises/blob/$branche{path}#{line}")
end

function créer_fichiers_documentation()
    dossier_racine = parent(cwd())
    dossier_source = p"source"
    chemin_gabarit = p"gabarit.md"
    chemin_index = p"index.md"
    copier_ressources(dossier_racine, dossier_source)
    pages_documentation = [
        "ŒuvresChinoises" => "œuvres chinoises.md",
        "Récupérateur" => "récupérateur.md",
        "Enrichisseur" => "enrichisseur.md",
        "Générateur" => "générateur.md",
        "Visualisateur" => "visualisateur.md",
        "Extracteur" => "extracteur.md",
        "Général" => "général.md",
        "Rapporteur" => "rapporteur.md",
        "Translittérateur" => "translittérateur.md",
        "Textes" => "textes.md",
        "BaseDeDonnées" => "base de données.md"]
    for page_documentation ∈ pages_documentation
        contenu = read(chemin_gabarit, String)
        contenu = reduce(replace, ["“module”" => page_documentation.first, "“fichier”" => page_documentation.second]; init=contenu)
        write(dossier_source / page_documentation.second, contenu)
    end
    return (dossier_source, chemin_index, pages_documentation)
end

function copier_ressources(dossier_racine::SystemPath, dossier_source::SystemPath)
    rm(dossier_source, force=true, recursive=true)
    mkpath(dossier_source)
    cp(dossier_racine / "README.md", dossier_source / "index.md", force=true)
    modifier_fichier_index(dossier_source / "index.md")
    mkpath(dossier_source / "exemples")
    cp(dossier_racine / "exemples", dossier_source / "exemples", force=true)
    mkpath(dossier_source / "résultats" / "publications")
    cp(dossier_racine / "résultats" / "publications", dossier_source / "résultats" / "publications", force=true)
end

function modifier_fichier_index(chemin::SystemPath)
    contenu = read(chemin, String) |> ajouter_date_actuelle
    write(chemin, contenu)
end

function ajouter_date_actuelle(contenu::AbstractString)::String
    maintenant = now()
    Dates.LOCALES["français"] = Dates.DateLocale(
        ["janvier", "février", "mars", "avril", "mai", "juin", "juillet", "août", "septembre", "octobre", "novembre", "décembre"],
        ["janv", "févr", "mars", "avril", "mai", "juin", "juil", "août", "sept", "oct", "nov", "déc"],
        ["lundi", "mardi", "mercredi", "jeudi", "vendredi", "samedi", "dimanche"],
        [""]);
    jour = Dates.dayofmonth(maintenant) ≠ 1 ? Dates.dayofmonth(maintenant) : "$(Dates.dayofmonth(maintenant))er"
    date = """$(Dates.dayname(maintenant; locale="français")) $jour $(Dates.monthname(maintenant; locale="français")) $(Dates.year(maintenant)) à $(Dates.hour(maintenant)) h $(Dates.format(maintenant, "MM")) ($(maintenant))."""
    contenu *= "\n# Dernière mise à jour\n\nLe $date\n"
end

cd(() -> créer_documentation(p"documentation"), "docs")
