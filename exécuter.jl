using ŒuvresChinoises
using FilePaths
using ŒuvresChinoises.Général

@time informations_générales = préparer();

@time informations_œuvres = répertorier_œuvres();
# écrire(informations_œuvres, p"œuvres.yml")
@time œuvres = récupérer_œuvres(informations_œuvres);

@time générer_rapport();

@time tableaux = enrichir_base(œuvres);

@time tableaux = récupérer_tableaux();
@time résultat = interroger()

résultats_à_transcription_manquante = filter(dyade -> dyade.titre_transcrit_œuvre_2 |> ismissing, résultat) |> simplifier

résultats_renards = filter(dyade ->
    (dyade.texte_œuvre_1 |> !ismissing &&
    any(occursin.("renard", dyade.texte_œuvre_1)) ||
    (dyade.titre_œuvre_1 |> !ismissing &&
    any(occursin.("renard", dyade.titre_œuvre_1)))) ||
    (dyade.texte_œuvre_2 |> !ismissing &&
    any(occursin.("狐", dyade.texte_œuvre_2)) ||
    (dyade.titre_œuvre_2 |> !ismissing &&
    any(occursin.("狐", dyade.titre_œuvre_2)))), résultat)

exporter(résultats_renards, "résultats/publications/renards.csv")
@time générer_analecta(résultats_renards)

résultats_test = filter(dyade -> dyade.titre_œuvre_1 == "Grâce", résultat)
@time générer_analecta(résultats_test, "Grâce")

@time visualiser(résultats_renards)
