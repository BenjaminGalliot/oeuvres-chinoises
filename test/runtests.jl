using FilePaths
using Pkg
using Test

push!(LOAD_PATH, parent(cwd()))

cd(() -> include("textes.jl" |> string), parent(cwd()))
cd(() -> include("translittérateur.jl" |> string), parent(cwd()))
cd(() -> include("ensemble.jl" |> string), parent(cwd()))
