using ŒuvresChinoises.Translittérateur

@testset "types" begin
    @test extranslittérer("Tchouang-tseu"; séparateur=" ") == "Zhuang zi"
    @test extranslittérer("Lie tseu"; séparateur="") == "Liezi"
    @test intranslittérer("Zhuang zi"; séparateur=" ") == "Tchouang tseu"
    @test intranslittérer("Liezi"; séparateur="-") == "Lie-tseu"
end
