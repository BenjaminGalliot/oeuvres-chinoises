using ŒuvresChinoises
using ŒuvresChinoises.Structures
using DataFrames
using FilePaths

@testset "traitement global" begin
    informations_générales = préparer();
    @test informations_générales isa Dict
    @test haskey(informations_générales, "langues")
    @test !isempty(informations_générales["langues"])
    @test haskey(informations_générales, "sources")
    @test !isempty(informations_générales["sources"])
    informations_œuvres = répertorier_œuvres();
    @test informations_œuvres isa Vector{Dict}
    @test (!isempty).(informations_œuvres) |> all
    œuvres = récupérer_œuvres(informations_œuvres);
    @test œuvres isa Vector{Œuvre}
    rapport = générer_rapport();
    @test rapport isa SystemPath
    tableaux = enrichir_base(œuvres);
    @test tableaux isa Dict{String, DataFrame}
    @test tableaux |> values .|> !isempty |> all
    tableaux_bis = récupérer_tableaux();
    @test (tableaux |> keys .== tableaux_bis |> keys) |> all
    @test (tableaux |> values .|> dropmissing .== tableaux_bis |> values .|> dropmissing) |> all
    résultat = interroger()
    @test résultat isa DataFrame
    fichier = exporter(résultat, "test.csv")
    @test fichier isa SystemPath
    fichier = générer_analecta(résultat)
    @test fichier isa SystemPath
end
