using ŒuvresChinoises.Général
using ŒuvresChinoises.Structures
using ŒuvresChinoises.Textes: récupérer_voisinage

@testset "types" begin
    langue_française = LangueFrançaise()
    @test langue_française isa LangueFrançaise
    langue_chinoise = LangueChinoise()
    @test langue_chinoise isa LangueChinoise
    source_picquier = SourcePicquier()
    @test source_picquier isa SourcePicquier
    source_ctext = SourceCtext(; clef="?")
    @test source_ctext isa SourceCtext
    source_indéfinie = SourceIndéfinie("?")
    @test source_indéfinie isa SourceIndéfinie
    auteur_français = Anthroponyme("André", "Lévy", nothing, nothing, langue_française)
    @test auteur_français isa Anthroponyme
    @test (auteur_français isa Anthroponyme{LangueAbstraite}) == false
    @test auteur_français isa Anthroponyme{<:LangueAbstraite}
    @test auteur_français isa Anthroponyme{LangueFrançaise}
    @test (auteur_français.langue = langue_française) isa LangueFrançaise
    @test_throws MethodError auteur_français.langue = langue_chinoise
    auteur_chinois = Anthroponyme("松齡", "蒲", "Songling", "Pu", langue_chinoise)
    @test auteur_chinois isa Anthroponyme{LangueChinoise}
    @test (auteur_chinois isa Anthroponyme{LangueFrançaise}) == false
    titre_français = Ergonyme("Chroniques de l’étrange", nothing, langue_française)
    @test titre_français isa Ergonyme
    @test (titre_français isa Ergonyme{LangueAbstraite}) == false
    @test titre_français isa Ergonyme{<:LangueAbstraite}
    @test titre_français isa Ergonyme{LangueFrançaise}
    @test (titre_français.langue = langue_française) isa LangueFrançaise
    @test_throws MethodError titre_français.langue = langue_chinoise
    titre_chinois = Ergonyme("聊齋誌異", "liaozhai zhiyi", langue_chinoise)
    @test titre_chinois isa Ergonyme{LangueChinoise}
    @test (titre_chinois isa Ergonyme{LangueFrançaise}) == false
    texte_français = Texte(; langue=langue_française, source=source_picquier)
    @test texte_français isa Texte
    @test texte_français isa Texte{LangueFrançaise, SourcePicquier}
    @test (texte_français.langue = langue_française) isa LangueFrançaise
    @test_throws MethodError texte_français.langue = langue_chinoise
    texte_chinois = Texte(; langue=langue_chinoise, source=source_ctext)
    @test texte_chinois isa Texte
    @test texte_chinois isa Texte{LangueChinoise, SourceCtext}
    @test (texte_chinois.langue = langue_chinoise) isa LangueChinoise
    @test_throws MethodError texte_chinois.langue = langue_française
    œuvre_française = Œuvre(; renseignement="?", langue=langue_française, source=source_picquier)
    @test œuvre_française isa Œuvre
    @test (œuvre_française isa Œuvre{LangueAbstraite, SourceAbstraite}) == false
    @test œuvre_française isa Œuvre{<:LangueAbstraite, <:SourceAbstraite}
    @test œuvre_française isa Œuvre{LangueFrançaise, SourcePicquier}
    @test (œuvre_française isa Œuvre{LangueChinoise, SourcePicquier}) == false
    @test (œuvre_française isa Œuvre{LangueFrançaise, SourceIndéfinie}) == false
    @test (œuvre_française.langue = langue_française) isa LangueFrançaise
    @test_throws MethodError œuvre_française.langue = langue_chinoise
    @test (œuvre_française.source = source_picquier) isa SourcePicquier
    @test_throws MethodError œuvre_française.source = source_ctext
    @test (push!(œuvre_française.auteurs, auteur_français)) isa Vector{<:Anthroponyme{LangueFrançaise}}
    @test_throws MethodError push!(œuvre_française.auteurs, auteur_chinois)
    @test (œuvre_française.titre = titre_français) isa Ergonyme
    @test_throws MethodError push!(œuvre_française.auteurs, auteur_chinois)
    @test (œuvre_française.texte = texte_français) isa Texte{LangueFrançaise}
    @test (œuvre_française.texte = nothing) isa Nothing
    @test_throws MethodError œuvre_française.texte = texte_chinois
    œuvre_chinoise = Œuvre(; renseignement="?", langue=langue_chinoise, source=source_ctext)
    @test œuvre_chinoise isa Œuvre{LangueChinoise, SourceCtext}
    @test (œuvre_française.parent = œuvre_française) isa Œuvre{LangueFrançaise}
    @test_throws MethodError œuvre_française.parent = œuvre_chinoise
    @test (push!(œuvre_chinoise.enfants, œuvre_chinoise)) isa Vector{<:Œuvre{LangueChinoise}}
    @test_throws MethodError push!(œuvre_chinoise.enfants, œuvre_française)
end

@testset "textes" begin
    expression = "孔生雪笠，聖裔也。為人蘊藉，工詩。"
    @test expression[19] isa Char
    @test récupérer_voisinage(expression, 19, 2, affiner=false) == "，聖裔也。"
    @test_throws StringIndexError expression[20]
    @test length(Set(récupérer_voisinage.(expression, [19, 20, 21], 2, affiner=false))) == 1
    @test récupérer_voisinage(expression, 19, 3, affiner=false) == "笠，聖裔也。為"
    @test récupérer_voisinage(expression, 19, 3, affiner=true) == "孔生雪笠，聖裔也。為人蘊藉，"
    expression = "Le bachelier Kong Xueli, de la sainte descendance de Confucius…"
    @test récupérer_voisinage(expression, 21, 2, affiner=false) == "Xueli"
    @test récupérer_voisinage(expression, 21, 2, affiner=true) == " Xueli,"
    @test récupérer_voisinage(expression, 21, 2, affiner=true) == récupérer_voisinage(expression, 21, 3, affiner=false)
    @test récupérer_voisinage(expression, 21, 4, affiner=false) == "g Xueli, "
    @test récupérer_voisinage(expression, 21, 4, affiner=true) == " Kong Xueli, "
end
